export enum CircleType {
  Default = 'default',
  Button = 'button',
  Link = 'link',
}

export enum CircleModifier {
  Base = '',

  /**
   * colors
   */
  ClrPrimary = 'primary',
}
