export enum GridColumnModifier {
  None = '',

  Auto = 'auto',
  AutoDesk = 'auto--desk',
  ShrinkWrap = 'shrink-wrap',
  ShrinkWrapDesk = 'shrink-wrap--desk',
  Tall = 'tall',
}

export enum GridWrapModifier {
  Base = '',

  // gutter size
  Smaller = 'smaller',
  Small = 'small',
  Large = 'large',
  Larger = 'larger',

  // alignment
  Middle = 'middle',
  Center = 'center',
  End = 'block-end',
}

export enum DisplayLevel {
  Block = 'block',
  InlineBlock = 'inline-block',
}
