# Usage instructions
1. Install ansible-playbook, if you haven't already. 
   See [this page](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) for installation instructions.
2. Copy `inventory.yml.example` to `inventory.yml`
3. Set the value of `ansible_host` (can be the ip address from terraform) and `gitlab_root_password`
4. In a shell, run `ansible-playbook -i inventory.yml --private-key <KEY> playbooks/gitlab.yml`. `<KEY>` is the file path 
   of the `.pem` copied during the server setup using terraform.
5. Check gitlab is installed by going to `http://<IP Address of gitlab server>`
 