# Server setup
Set the following environment variables:
```bash
export TF_VAR_openstack_gitlab_username=
export TF_VAR_openstack_gitlab_password=
export TF_VAR_openstack_gitlab_project_domain_name=
export TF_VAR_openstack_gitlab_project_domain_id=
export TF_VAR_openstack_gitlab_keypair_name=
```
**Note** The `.pem` file for the value set in `TF_VAR_openstack_gitlab_keypair_name` needs to have been copied somewhere 
safe for use by `ansible-playbook`. Remember to give the file appropriate permissions e.g `chown 400 key.pem`

In `infranstructure/terraform/dev` run:
```bash
terraform init
```
then
```bash
terraform plan
```
if everything looks good:
```bash
terraform apply
```

Once the server has been deployed, you'll see the ip address of like so:
```bash
....
Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

Outputs:

gitlab_instance_ip = "147.135.195.204"

```
The IP address can be used with ansible