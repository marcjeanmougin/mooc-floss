terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.37.0"
    }
  }
  required_version = ">= 0.14.0"
}

variable "openstack_gitlab_username" {}
variable "openstack_gitlab_password" {}
variable "openstack_gitlab_project_domain_name" {}
variable "openstack_gitlab_project_domain_id" {}
variable "openstack_gitlab_keypair_name" {}

locals {
  openstack_gitlab_username            = var.openstack_gitlab_username
  openstack_gitlab_password            = var.openstack_gitlab_password
  openstack_gitlab_project_domain_name = var.openstack_gitlab_project_domain_name
  openstack_gitlab_project_domain_id   = var.openstack_gitlab_project_domain_id
  openstack_gitlab_keypair_name        = var.openstack_gitlab_keypair_name
  openstack_region                     = "GRA3"
}

provider "openstack" {
  auth_url            = "https://auth.cloud.ovh.net/v3"
  user_domain_name    = "Default"
  user_name           = local.openstack_gitlab_username
  password            = local.openstack_gitlab_password
  region              = local.openstack_region
  project_domain_name = local.openstack_gitlab_project_domain_name
  project_domain_id   = local.openstack_gitlab_project_domain_id
}

module "gitlab_server" {
  source = "../modules/server"

  providers = {
    openstack = openstack
  }
  key_pair_name = local.openstack_gitlab_keypair_name
}

output "gitlab_instance_ip" {
  value = module.gitlab_server.instance_ip_addr
}