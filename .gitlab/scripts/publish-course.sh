#!/usr/bin/env bash
# This script does:
#   - Retrieve an oAuth2 access token from Open edX
#   - Compress the course into a .tar.gz file
#   - Upload the archive to the Open edX courses catalog
# API documentation: https://course-catalog-api-guide.readthedocs.io/en/latest/
# Requirements:
#   - curl
#   - jq
#   - bash
#   - gnutar/bsdtar (not busybox tar)
# Environment variables:
#   - EDX_STUDIO_URL
#   - EDX_LMS_URL
#   - EDX_AUTH_CLIENT_ID
#   - EDX_AUTH_CLIENT_SECRET
#   - COURSE_ID
set -euo pipefail

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__repo="$(realpath ${__dir}/../..)"
__course_path="$(realpath ${__repo}/course)"
__bsdtar="$([[ $(tar --help) =~ 'bsdtar' ]] && echo 1 || echo 0)"
__studio_url="${EDX_STUDIO_URL}"
__lms_url="${EDX_LMS_URL}"
__auth_url="${__lms_url}/oauth2/access_token"
__client_id="${EDX_AUTH_CLIENT_ID}"
__client_secret="${EDX_AUTH_CLIENT_SECRET}"
__course_id="${COURSE_ID}"
__access_token=""

function check_command {
    if ! command -v ${1} &> /dev/null; then
        log "${1} command could not be found"; exit
    fi 
}

function log {
    echo "[$(date -u)]: $*"
}

check_command curl
check_command jq

log "Open edX LMS URL: ${__lms_url}"
log "Open edX Studio URL: ${__studio_url}"
log "Open edX Client ID: ${__client_id}"
log "Open edX Client secret: $(echo ${__client_secret} |cut -c1-5)*****"
log "Course ID: ${__course_id}"
log "Course path: ${__course_path}"
log "Using tar(bsdtar): $__bsdtar"

retrieve_access_token () {
    local response=$(curl \
        -X POST -v \
        -H "Accept: application/json" \
        -H "Content-Type: application/x-www-form-urlencoded" \
        -d "grant_type=client_credentials&client_id=${__client_id}&client_secret=${__client_secret}" \
        ${__auth_url})
    __access_token=$(echo ${response} | jq -j '.access_token')
    echo
    if [[ -z ${__access_token} || ${__access_token} = null ]]; then
        log "Error while retrieving an access token."
        log "Response body: ${response}"; exit
    fi
    log ${response}
    log "Access token: ${__access_token}"
}

compress () {
    local course_dir=${1}
    local course_path=${2}
    log "[${course_dir}] Archive output: $(realpath ${course_path}/../${course_dir}.tar.gz)"
    log "[${course_dir}] Archiving =============================="
    if [ ${__bsdtar} -eq 1 ]; then
        tar zcfv ${course_dir}.tar.gz -s '/${course_dir}/course/s' ${course_dir}/*
    else
        tar zcfv ${course_dir}.tar.gz --transform 's/${course_dir}/course/' ${course_dir}/*
    fi
    log "[${course_dir}] = END =================================="
}

upload () {
    local course_id=${1}
    local archive_path=${2}
    log "[${course_dir}] Uploading =============================="
    log "[${course_dir}] Archive path: ${2}"

    local response=$(
        curl -X POST -v \
        -H "Accept: application/json" \
        -H "Authorization: Bearer ${__access_token}" \
        -F "course_id=${course_id}" \
        -F "course_data=@${archive_path}" \
        ${__studio_url}/api/courses/v0/import/${course_id}/
    )
    echo $response
    log "[${course_dir}] = END =================================="
}

log "Retrieving access token..."
retrieve_access_token

course_dir="$(basename ${__course_path})"
log "[${course_dir}] Updating..."
log "[${course_dir}] Course name: ${__course_id}"
compress ${course_dir} ${__course_path}
upload ${COURSE_ID} ${course_dir}.tar.gz
log "Course studio URL: ${__studio_url}/course/${__course_id}"
log "Course lms URL: ${__lms_url}/courses/${__course_id}" 
log "[${course_dir}] Done."
