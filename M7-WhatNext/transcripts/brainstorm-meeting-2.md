# Brainstorm - Intro/outro - Meeting 2 Transcript

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1621875584636

Marc: 00:00:01.271 Should I repeat what I just said, or?

Xavier: 00:00:03.742 I think we got it from the previous session, but just to make sure that we capture that. You're good.

Marc: 00:00:13.468 Okay. So we have two things to do today: the introduction and the conclusion of the MOOC. So, yep.

Xavier: 00:00:30.562 How do you see those sections, by the way? Do you see them as the same size as the other modules, something shorter?

Marc: 00:00:39.909 No, I think it should be much shorter, like the introduction should be something like where it shouldn't feel like it's a MOOC. If they are used to MOOC, it should feel like a MOOC in the introduction, like a presentation of who is going to teach them, what is the structure, what they are going to learn, what is the prerequisites, so that they should feel in good hands and feel safe, feel like they will want to learn what we are going to teach in the whole MOOC. And it should start-- I think it should start to engage them to talk among themselves and with us. So introduce themselves to each other and to us, and try to learn who the MOOC creators are and what is the spirit of the MOOC, like how it was created, maybe? I'm not convinced that how it was created is a good thing to put in the introduction and not in the conclusion, but that's something we can discuss.

Marc: 00:01:59.842 And for the conclusion, since we ended the previous week with basically, "Submit a fix to a bug and peer grade others that have done so," most of the technical tasks of the MOOC-- all the technical tasks of the MOOC have been done, and it's just, "Repeat that and contribute to open-source as much as you can or as much as you want." And that's basically it, and we can open and, I don't know, many things. But I don't know what's desirable at this point. We can say that they haven't learned everything there is to know about the open-source world like there are different possibilities, different worlds they can evolve into.

Kendall: 00:02:57.196 Yeah. I think some sort of disclaimer, where you're like, "Obviously this doesn't cover all of the tools and all of the things that you'll learn in open-source, but it should be enough to get you started in a wide variety of communities." And then, you can also be like, "If you'd like to contribute back to this training, we also really appreciate any help that you can give us," either helping moderate the activities, or if there's content they want to add, or that sort of thing, too.

Marc: 00:03:31.277 If you want to become a mentor for future students to taking the MOOC or things like that?

Kendall: 00:03:37.515 Yeah.

Xavier: 00:03:37.674 Would that be in the introduction or the conclusion?

Marc: 00:03:41.623 Conclusion.

Kendall: 00:03:42.203 Conclusion. Yeah.

Xavier: 00:03:44.623 Because at least, for contributing to the MOOC, that's something that will also want to encourage why they go through the MOOC, right? Because that's when they see issues. But I guess some of that might end up being partially in the intro, partially in the conclusion or at least as a reminder or something.

Kendall: 00:04:07.578 Yeah. I would also put the disclaimer thing about this covering a large portion, but not all of the ins and outs of tools and processes and whatnot for open-source communities. I would say that at both in the introduction and at the end as a reminder too.

Xavier: 00:04:25.019 Yeah. To manage expectations when they start. Makes sense. And, Marc, you were mentioning for the intro that it could be interesting to mention how the MOOC was created. I would agree that it's an important one because we [cannot?] try to apply the recipes of the MOOC to the conception of the MOOC. It was done as a community [inaudible] and as a community effort. And it involves multiple communities. So maybe even presenting those communities, those organization, the different people, I think it helps because then you know who you are talking to. You know also a little bit what to expect. That it might have a diversity of point of view. It might not be one project. It might not be all the project, but still there will be some diversity.

Marc: 00:05:22.996 Mostly I feel like we have already a great variety of actors in the open-source world. You run a start-up to doing open source. We have Ildiko and Kendall from a big foundation of a huge open-source project. I'm mostly a maintainer of a purely community project, which is only voluntary contribution, like--sorry, I forget the word, but non-paid contributors. Polyedre is a student wanting to contribute to open source, and Remi is mostly drive-by on projects he's interested in. So we do have some diversity of profiles, and I think it's important to mention that.

Xavier: 00:06:26.132 Yeah. Definitely. So would you see also, a presentation of the different people in there, or how would that work?

Marc: 00:06:35.231 Yeah. We could do that. I don't know how it will evolve in the future because if we get other people to-- if the list of contributors grows, and we cannot grow infinitely the list that we present of contributors to the MOOC. But we can definitely list the people who were involved before launching the MOOC, and the people who actually contributed to create the syllabus and to create the content and this is the core team of the MOOC in its starting state [crosstalk]--

Kendall: 00:07:14.029 But you want to have, I don't know, a page with our pictures and like a short bio or something like that, just for them to look at, or?

Marc: 00:07:25.448 Yeah. I think it's quite frequent in MOOCs to have this kind of page at the beginning, in addition to the page where they present the-- the description of the MOOC also has a quick description of people, but also in the MOOC we can be more detailed about from which point of view we talk and who we are, and.

Kendall: 00:07:49.270 Okay. Yeah. That makes sense. [crosstalk].

Marc: 00:07:51.606 And also why we are involved in this and why we are interested in them learning all that.

Kendall: 00:07:57.497 Yeah. Cool. I've never actually taken a MOOC, so [laughter] there's that fun context that I didn't have.

Xavier: 00:08:09.957 Well, I think that point could even help build some rapport with the students especially if we present ourselves in a video and etc. It shows that it's not just like a lesson or a book. Like people, there's a community around it. And getting to know the people involved a bit personally is kind of nice. Yeah. I like that also in MOOCs we are a little bit more personable. That's usually nicer.

Marc: 00:08:41.535 Well, if they think it's going to be a lesson MOOC, they will be for a surprise very early in the MOOC.

Xavier: 00:08:54.775 That is true.
[silence]

Marc: 00:09:11.905 Okay. So how do we plan that?

Xavier: 00:09:24.916 That's a good question. Do we present the structure also of the course like the different weeks, the evolution, what to expect, the fact that this will be heavily based on actually them doing things because you can't learn to contribute [directly?]? Maybe kind of giving a preview of those things.

Marc: 00:09:49.719 I would call it managing expectations. Because if they come because they're interested in the subject but don't really have time, we can tell that it will be very, very difficult. They can follow everything and get all the knowledge passing element, but they will do way less than half of the MOOC if they do not contribute. If they only listen passively to the MOOC basically. We also can give the structures, so that's the list of things we will talk about in the chapters because will allow the people who are only interested in part of the MOOC to skip and only-- to skip what's before and just go to listen and then drop off the MOOC if they want to. Which I'm not opposed to having people who want to drop off the MOOC and just have some knowledge of part of what we want to teach. And it will just make them gain time compared to not knowing where is what in the MOOC and just skipping slowly.

Xavier: 00:11:11.999 Yep. That makes sense to me.

Marc: 00:11:14.777 I mean, I don't know if it's worth it to try to keep in the MOOC people who are only interested in a tiny part of the MOOC.

Xavier: 00:11:25.176 No, I mean, they can do it if they want to, but I think it doesn't prevent us to be at least clear on what are the expectations, what they need to do to get the most of it because it's not even [inaudible] it's also even the rewarding part like getting a contribution actually in the project, getting some relationships and etc. It's important also maybe to show that it's not just work. There is something to get from that and something that you can't completely imagine unless you have done it. So yeah, I think trying to keep people just for the sake of it like if you try to please everyone you'll please no one, so I'd rather really focus on what is important and how we have actually built the course which is like people need a little bit of motivation because actually to contribute you need a bit of motivation. It just doesn't happen like that, so yeah. Do we-- sorry, go ahead. No. Do we want to give an idea of the effort involved like the number of hours, the time, those kinds of things? Do we even have an idea on that, [honestly?]?

Marc: 00:12:49.822 There was a [column?] for that in the table, but we sort of did not fill it after the first week.

Xavier: 00:13:01.527 Especially because there is a lot of work that people have to do on their own with the project, and this can be like five minutes to two weeks.

Marc: 00:13:10.240 Yeah. But it is necessary I think when we put up a MOOC to give an idea of the time involved and most of time involved in the MOOC itself and the time involved in terms of personal involvement. And the MOOC time is usually both times. And that's a good point that we should try to recalculate that and [see?].

Xavier: 00:13:42.983 Yeah. What was that giving for the first week? Because I think clearly our MOOC falls on the heavier side just because of the amount of work that people have to do not necessarily because of the theory. And that can easily-- sorry about that. They can easily go into like 8 to 10 hours per week. I mean, maybe we can try to target a little bit less than that for our people who don't have too much time. But it's going to take some time every module in any case.

Marc: 00:14:19.469 The first estimate I had before was like around 24 hours total. Now, it used to be like 4 hours per week for seven weeks. I think now we have six modules. So it's six times something. But first modules are faster I think because they do not involve discussions with project already in the first and second one I think.

Xavier: 00:14:57.274 They do actually.

Marc: 00:14:58.544 Okay. Yes. Sorry.

Xavier: 00:15:04.185 I mean, it's not a lot necessarily every week. So they're not going to be expected to be spending 10 hours talking with people. But it's human interaction so it's, yeah.

Marc: 00:15:16.035 It takes time. The first week in the expected time takes like three hours. And the second week is like 2 hours. And then we stopped counting it. But I think it will be more for all the rest. And it will depend also on what they choose to do because fixing a bug is not in a fixed amount of time.

Kendall: 00:15:49.394 Yeah. A lot of them are going to be variable based on the project they select and stuff like that.

Xavier: 00:15:59.314 Yeah. I guess maybe that would be a good question for Loic because he did it in synchronously, and I think he had pretty much a count of that time. And he had less theory than we do, so. And I think he was doing like at least two, three days synchronously plus the mentoring afterwards. So definitely I think we're already above what you were counting. And actually what about on your OpenStack side? How long does the workshop last? Is it one day?

Kendall: 00:16:38.922 Typically it's a day and a half. We start with a half day, and then we do a whole day the next day. But a couple events. We've had to kind of compress down and do a single day. So it's like a very, very, full eight hours.

Xavier: 00:16:53.154 Oh, wow. I imagine. And given you've seen of the content that we've put in the course because that probably gives you a difference compared to what you were trying to cram into one day, how does it feel in terms of volume overall?

Kendall: 00:17:14.604 I'd say it's pretty similar. But because the topics are a little bit more generalized which kind of makes them feel less heavy than all of the stuff that we go through for the OpenStack one. But at the same time, I think that we cover more breadth across open source in general in this MOOC. So maybe slightly more, not a ton. If you were doing synchronously at least you could probably do it in a few days. But yeah, as far as it being self-paced and stuff, like a few hours per module I think is probably about right.

Xavier: 00:18:06.651 Yep. Okay. So yeah, that seems to be consistent across the board. I think we'll probably be above 5 hours per week. Then how much we say exactly like maybe 5 to 8 or something like that. I don't know. We'll have to give a range probably anyway.

Kendall: 00:18:28.021 Yeah, definitely a range and not, "This will take 2 hours." [laughter]

Marc: 00:18:36.449 Anyway, we call them modules so that they can span it over weeks if they want to or do everything in the same week if they can.

Xavier: 00:18:48.877 Yeah, I could separate that, but, yeah.
[silence]

Xavier: 00:19:02.140 And should we also present the different type of sections? So like the fact that there is a theory part, there is a project tip part. Even in the project tips, we have kind of like the diary concept. So maybe those are things that will be recurring through the course that we can introduce, or should we wait for module one?

Marc: 00:19:26.410 I think for the diary-- we can introduce the project tips even at the beginning because I think during the introduction we will clearly say that they are expected to produce a fix for a bug in an open-source project. So we can call that the project and say there will be tips for that at the end of each module. So I think that's something worth mentioning and worth introducing at the beginning. For the diary, I'm not convinced, and I think it would be good to leave the introduction of that when it's actually created because it fits more in the topic of the course on Git, basically. We will say, "We will use Git to do a diary of your progress on the project," and you'd have to create it.

Kendall: 00:20:23.467 Yeah. I think that makes sense. If you introduce activities like that too early, people might try to start it early. And so if we're trying to kind of keep everybody on the same page, it's probably better to wait.

Xavier: 00:20:38.606 Makes sense.

Marc: 00:20:39.106 Because if they try to create it too early but are not really sure what to do, it might be an issue. They will say, "Oh, I'm expected to do a diary, but I don't really know what it means, where to create it or how," and if we go into that, then we go into the subject of the week two-- of the module two, and.
[silence]

Xavier: 00:21:24.004 Should we also present the organization involved because we talked to other people whether it's IMT, OpenStack, Open edX? Well, module one we have introduction of projects, and I guess, but not projects for people who want to get involved and don't know which one. So I don't know if that's redundant. But at the same time, it's pressuring.

Marc: 00:21:53.273 I think it fits into the, "Who are we?" topic. Like when we introduce ourselves, we can say, "We are employed by whom." The part of who we are is, what do we have to do with open source, and what link do we have, and basically, why are we here? And I think we can introduce our companies/foundation/projects in the part where we discuss about us.

Kendall: 00:22:27.490 Yeah, I think that sounds good.

Xavier: 00:22:33.078 That makes sense.

Polyedre: 00:22:35.756 What about the tools, for instance GitLab instance or [dependent end user?] tools. But what about the tools that will be available on the forum, for instance?

Marc: 00:22:52.173 You want us to present them before they are used, like saying, "We'll use this tool and this tool"?

Polyedre: 00:22:58.332 Yeah. Or maybe just explain that, "This tool will exist," even if it's two sentences.

Xavier: 00:23:10.323 Well, one thing advocating for this would be the fact that we want to encourage them to contribute to the course. And if we say, "Okay, it would be great if you contribute to the course," it might be good to say, "Okay. The source code of the course is there. If you want to comment on the forum, it's there." So maybe giving, like an overview of the different places that people can go to. I don't know if we'll have, I guess, the chat room, if we might want the students to come in there to chat. These kind of things might make sense to do in the first week. I mean, we are talking about the tools, in general, in the coming weeks, but maybe at least presenting the one from the project might make sense.

Marc: 00:24:03.730 I think what Polyedre is mentioning is to basically say to students, "In the coming weeks you will have to use this tool and this tool and this tool." So that they can know in advance what tools they will use and what they will not use if they know some.

Xavier: 00:24:23.996 Sure. But then you do kind of both at the same time by telling them what the courses use. Or do you mean also tools that might be used on the project side? Like on the side of their contribution?

Marc: 00:24:42.434 No. I mean they use tool as students like Minetest or GitLab. And okay, in this case it's the same, but it could be different than the tools that we use to create the course. So not only presenting the tools that we use to create the course, but the tools that they will use as students during the course. It's two things.

Xavier: 00:25:13.195 Yeah, it's true. It is a bit different. But then what would be the advantage of showing those if they are also going to see it in the course?

Marc: 00:25:28.454 Just like spoiler.

Xavier: 00:25:36.005 But why not? But--

Marc: 00:25:38.478 We don't have to, of course.

Xavier: 00:25:40.144 Now again, on my side I see a case to show them where they can contribute in the context of the course because again we want to encourage that. And I guess, if there was a reason for them to be aware in advance of some of the course-- of some of the tools, that might make sense. But if we just list them all without a specific goal for that, I don't know if that's--

Polyedre: 00:26:17.940 In the introduction maybe we will ask them to introduce themselves. So maybe presenting the forum at least will be required.

Xavier: 00:26:30.096 For sure. Well, that would fall in the category of the tools used to kind of-- I mean, it's not to build the course, but it's where they can contribute to the course. Because that's the community aspect of it, I guess. So yeah, the forum, that would make sense. Again, I think that the chat would make sense because that's kind of the synchronous equivalent of the forum, so it might be good for them to have a place to chat. Yeah, you mentioned the GitLab instance, although I guess that's not exactly the same as where the source of the course is. That's still GitLab, but it's not the same.
[silence]

Polyedre: 00:27:27.677 Yeah. So maybe in the introduction, just explain that there is a forum if they need help for instance. And that the MOOC is open source, so they can see how it is written and how people contribute to it. Maybe it can be fun. As a student, I would be interested even before beginning the MOOC to just have a look to how it works behind the scene.

Xavier: 00:28:05.062 I agree. Well, especially because it's people interested by contributing to open source, so we can imagine that most people like you would be curious of that. And to me, that would also be a signal that I am within a community who actually believe in open source and is not just trying to make money out of [open source?], if that makes sense, so. I mean not that there's a problem out of making money out of open source, but that is not the only driver. And it is also people doing that properly like in a real open-source project.

Marc: 00:28:42.900 That could also be apparent in the presentation of who we are and where we are, yeah.

Xavier: 00:28:49.199 True. But it's good if on top of what we say and what we announce there's an actual proof by showing, yeah, like, "Here is the code," or, "Here is the license," or, "You can see all our discussions here." I think that's a strong [signal?] because a lot of people say all of that. And then you're like, "Where is the code?" "We'll publish it one day," that kind of--

Marc: 00:29:14.776 I know. Like [inaudible].

Xavier: 00:29:18.905 Yeah.

Marc: 00:29:27.885 We can say, "Please do not look at the quiz definition in the source code because the answers are in there."

Xavier: 00:29:35.045 Well, they can. I mean, the ultimate test will be peer grading, so they can't really cheat on that one, but.

Marc: 00:29:41.774 Of course. Except if you go into the edX source code, and you find how to escape peer grading and just grade yourself.

Xavier: 00:29:56.072 Well, yeah, but you need access to the edX instance, then good luck with that.
[silence]

Xavier: 00:30:14.019 One of the other points also is that the introduction is also a moment where we try to kind of convince the students to stick with us. I know that's one thing that I do often when I get into a MOOC. Actually, I will watch the first few videos or sections of a lot of MOOC and keep very few of them for that. So it's kind of first page of a book, first few pages. There needs to be something convincing. So on top of the stuff we have mentioned, there is something appealing, I think, that comes back through the course which is interviews and having like figures, names, and etc. So it might be good also to keep that in mind while we do the interviews where we [list?] the questions is to have things that we could include in the intro that would be appealing, "Oh, I want to know more. What is that person going to say about that?" Something that might make people want to, "Okay, when is the next time that I don't know who is going to talk?" And then they'll follow some part of the course just to see that next [separation?] or something.

Marc: 00:31:32.296 That reminds me, there is another thing we will have to do for the MOOC. It is a presentation video like marketing video for the MOOC, and that will also have its place in this kind of [inaudible].

Xavier: 00:31:44.097 For sure. And actually, I don't know that's probably not the right space to talk about this, but we'll need to figure out video quality for the interviews, I guess, because we are probably not going to be able to send them all to Paris to be doing it in the studios or something like that. So we might have to make sure they have at least a decent webcam and microphone, these kind of things. I mean, it's probably not a discussion for now, but that's something to keep in mind, I think. Especially if we want to have them in the marketing videos and etc., we can't have really crappy Zoom compressed crap.

Kendall: 00:32:35.868 It's funny how long video calls have been a thing, and how many of them are still just absolute garbage. [laughter]

Xavier: 00:32:44.827 Agreed. You would think people would have iterated a little bit on that, but no.

Kendall: 00:32:50.018 Right. How has technology not come that far yet?

Xavier: 00:32:55.157 Well, I think if you do some basic stuff, you can improve things a lot. Just having a decent microphone, a decent webcam, and a decent internet connection is already a pretty good start. A lot of nerds will have a decent internet connection, but webcam and microphone, that's a lot rarer actually, so.

Marc: 00:33:18.208 You need headphones?

Xavier: 00:33:20.848 Well, yeah. I mean, headphones, yeah, I guess. That's actually a question, will we want to-- if we want to have headphones on camera, I guess I [crosstalk]--

Marc: 00:33:31.077 No, no. You need headphones for synchronous communication when you have sounds that can go into the microphone. But if you are in an interview situation, you can have better, directed microphones that don't really keep all sounds. Like this microphone.

Xavier: 00:33:54.080 You could. I mean, like in e-sports or even sports, it's not unheard of to have people with their audio cast. Like if you watch any StarCraft competition, or even, I don't know, like Formula 1, because there is a lot of noise with the cars and whatever. So it's not completely absurd. And we are a bunch of geeks, so we don't need to look like the Eurovision or anything. But yeah, that's still a question, and we'll need to sort that out.
[silence]

Marc: 00:34:34.378 Okay. Remi is not here, so I cannot ask him the question. So I will ask you, Xavier. Do you see other things that are typically in an introduction of a course, [laughter] like in the first parts?

Xavier: 00:34:46.464 Well, that's what I've been trying to think since earlier. So I've found a few, but I guess, it would've been useful to do a sample of the introductions for all or just ask Remi. Remi is a good reference for that. Now I guess there is [inaudible] in grading, so what do you need to pass the course?

Marc: 00:35:10.091 We lost you for five seconds, so.

Xavier: 00:35:12.493 Oh. Sorry. So yeah, I was saying that one of the other things that is often there is the explanation of the grading, so what do you need to pass the course? Do you get a certificate? Do you get to pay or not? Maybe if we have mentoring and these kind of things, maybe that's something we want to explain there. But yeah, at least, yeah. Learning objective and passing grade and exams and etc. might be a useful one.

Marc: 00:35:47.005 Yep. Definitely. Yeah. Basically, also the, why should you pay for mentoring or for the certificate? What will you get in exchange for that? So that we can be clear that most of the actual content will be there even if you don't pay, within the structure imposed by edX because there may be restrictions in time or I don't--

Xavier: 00:36:20.653 Yep. I mean, in any case, the content will be on the open-source license, so they will be guaranteed to be able to access it. But yeah, that's a good point. I think also telling why there is a paid version. I mean, with some of the stuff that might be more of use than of others, like for example the mentoring in person or whatever. But even for the course because it exists, it's open source, etc., why do I need to pay? Well, there is that fact that it will help improve it. Well, I guess, the certificate is quite common for MOOCs, so we probably don't need to explain a lot. But still, one of the goals from some of them might be to try to find a job in open source. So getting that might be good. But especially the fact that the goal of all the parties involved is not really to become rich with the MOOC but try to keep improving it to make sure that we get more people contributing in open source. So not only they help themselves with the certificate or whatever, but also they help making sure that the money is a contribution--

Marc: 00:37:49.800 You got cut, but I think at the end of the sentence.

Xavier: 00:37:52.870 Okay. Perfect.

Marc: 00:37:54.041 You got cut after money is a contribution.

Xavier: 00:37:57.972 Yeah, that was it. That was the end. Perfect.

Marc: 00:38:06.561 Okay. So I think most of the things for the intro are maybe not in order but put into the table. So greetings. Who are we? Syllabus. How to pass basically. Introduce yourselves and talking about the project. Project tip number zero is, there is a project. And then for the conclusion, which might a bit trickier. We might have different-- so I think the first thing to do after the previous week is to congratulate them and tell them that they did something [not obvious?] and good for everyone because they contributed to something that everyone on earth can use, and.

Xavier: 00:39:12.500 Well, hopefully, their contribution was accepted though. That might be a [inaudible] question [mark?], even point some cases. So we might need to take that into account, I guess.

Kendall: 00:39:25.286 I mean you can still congratulate them on getting to the point and stuff.

Xavier: 00:39:28.124 Of course. Yes, that's what I meant is that we need to be mindful of that when we mention it to make sure that we value what they will get in any case, but.

Marc: 00:39:47.720 Yeah. In any case, if they communicated with the project and did fix something for that project, it will be appreciated by the project normally.

Xavier: 00:40:02.049 And that's already more than what 99% of people who use open-source every day will ever have done. And even that is already [inaudible]. I mean, even if they just finish the first module, go in a project [inaudible] for having done that and they stop there, it's already amazing because now, most people don't do that. So yeah. I guess the other part then is the segue on what comes next. Because obviously, that's only going to be one of the first steps, and it takes a lot more work to become a contributor on the long [inaudible] and etc., but yeah.

Marc: 00:40:55.569 Not a lot more. If you already contributed some things then you did some most difficult topics, which is entering the community and knowing how the community operates and start to do something with them. And I think that's the most difficult part, and continuing on that is actually easier.

Xavier: 00:41:15.474 That is true. That's why we focus on that. It's because it's the part that blocks most people, and psychologically, it's definitely the hardest. But what I meant is that there's still a lot of legwork left. And it would be a mistake to stop the course, be like, "Okay, I've done my contribution and that's all, folks." It is literally the first step. And just like any good habits, it can take a while. Just like, I don't know, going to run one time or two is one thing, but running every day for years is a lot more difficult to take as a habit. But hopefully, they will have got some of the rewarding part, and. But maybe giving some reasons why they would want to do that, I mean, at least for the people who would be wondering. For some people, it will be obvious because they love the project. They at least started well, so it will flow. But for some of the people who will be like, "Okay, I am done." The fact that, yeah, the more they get involved in the community, the more they will actually build a group of friends, a family around them. For their careers, definitely, if they become a core contributor of that project or another project even, that's definitely-- valuable by far than just one isolated contribution. The fact that their skills is going to improve in the long term. Having a few reasons why they should continue.
[silence]

Marc: 00:43:22.429 Yep. Basically the MOOC covers an experience that is roughly that of a drive-by contributor. Even if most drive-by contributors are familiar enough in the whole process to be able to contribute to sort of any project that's open enough. But yeah, it's a different experience from a regular contributor to any project. And it's a different experience from that of maintainer or anything similar. I wrote some stuff at the end of the Calc if you want to check or add or whatever.

Xavier: 00:44:21.688 Yeah. That makes sense. It's the sections we've discussed. I think again to go back to interviews, it might be useful to have some questions around, "How was it to become core maintainer on the first project?" This kind of thing. Kind of echoing one of the first questions which was, "How was your first contribution?" But then to kind of give a glimpse that there is more and that maybe one day they might end up being one of the people interviewed on the MOOC or whatever if they keep going that way.

Marc: 00:45:12.369 Yeah. Basically, the first interview questions were, "How did you feel for your beginning in free-software community?" Then the end question could be, "How did you stay involved in the long run, and what did you feel for that?" Like, "How did you keep the motivation? How did you feel basically about that?" And, "How did you end up being famous?" It's just by staying around and being visible voice of the project or things like that. If you stay around enough then people ask you to give talks and talk about the project or what has been done recently because if you're the maintainer, you know the most about it and the--

Xavier: 00:45:59.288 Completely. Yeah. And what they like about that new position because there's [inaudible] that it represents. But yeah, having people already well installed into that saying, "Well, why did we?" when they probably could stop and they would still be fine. There might be a few interesting insights there.

Marc: 00:46:31.769 I have no idea how to finish a course. What students end with basically.

Xavier: 00:46:43.267 In what sense? What can be the last sentence of the course or something, or?

Marc: 00:46:51.798 Yeah. For instance, yeah. Or the last paragraph or the last discussions topics.

Polyedre: 00:47:02.434 Maybe some resources. Some links or openings.

Marc: 00:47:08.066 That's very academic, but yeah. [laughter]

Xavier: 00:47:11.837 Yeah, to go further. I mean why not? That is a good point. Maybe it could be just-- we could leave it to some of the interviews potentially. It depends what we get there, but that might be an inspirational sentence that someone famous gives or that a student gives for that matter. Doesn't have to be someone famous. Does it depend on the materials we'll get?

Marc: 00:47:44.169 Yep. We can ask the question, "What would you say to our students as a last word?" [laughter]

Xavier: 00:47:50.138 Yeah. I didn't know what to put there, so it's your--

Marc: 00:47:54.238 So I leave that to interviewees because that's easier.

Xavier: 00:47:58.904 Well, it's true. Maybe it could be what advice-- we had I think in the first module, what advice would you give to someone who is looking to contribute? Maybe it could be what advice would you give to someone finishing the course or having done their first contribution and looking to go a bit further? Maybe there might be some interesting thing [they had?].

Marc: 00:48:21.612 And we can put ourself into the interviews here, right?

Xavier: 00:48:26.604 We can put what? Sorry.

Marc: 00:48:28.393 Ourselves.

Xavier: 00:48:29.597 Oh, yeah, sure. Which is kind of what we're doing right now. You're asking the question, "What should we put at the end?" And we're trying to answer. And we are recorded, so maybe that's going to end up in the course. Who knows.

Marc: 00:48:45.551 We just take the [inaudible] on recording and just put this with bad quality webcams, and then none really specific backgrounds. [laughter]

Xavier: 00:48:58.742 Yeah. It's just authentic.

Marc: 00:49:09.443 Yeah, I have put-- or maybe Polyedre put a section, "Stay around the course," to keep to those who want to engage with the course with maybe to guide the new newcomers or to build the Minetest world or whatever.

Xavier: 00:49:33.047 Yeah, I think that will make a lot of sense.

Marc: 00:49:36.218 Which is not only contributing to the course content, but contributing to the course community is maybe as important, to keep activity in the forums, make sure people get answers, and so on?

Xavier: 00:49:53.121 For sure.
[silence]

Xavier: 00:50:20.130 I'm trying to think of other elements we could include.
[silence]

Marc: 00:50:56.711 So you have links and references. We can link many, many things that cover some parts of the course.

Xavier: 00:51:08.671 And those can be contributive also.

Marc: 00:51:11.870 Yeah.
[silence]

Marc: 00:51:24.106 Maybe ask for feedback somehow? I don't know if that's done by a form or forum topic, or. I don't know if there is a standard way to do it on MOOCs like a, "Did you like it? What did you want to change?" etc.

Xavier: 00:51:44.026 People do forms a lot, but I think in the context of a community and open source, having it as a forum thread seems more appealing to me because then people can also see transparently, interact, answer, see what comes back often more easily.

Marc: 00:52:04.938 Yeah, but with a forum thread it's hell to summarize and to compile the results, basically.

Xavier: 00:52:15.818 For sure. Because it's more free form.

Marc: 00:52:21.177 Yeah. But even if we give a template of questions to answer, the fact that it's a list of posts makes it very hard to count the number of people who say, "I'm not happy," or "I'm happy," basically.
You have a rough idea.

Xavier: 00:52:38.414 Well I guess there could always be both. You could have like a quantitative form or even a poll or something in line in the course which allows you to get that kind of quantitative aspect. And for the more qualitative answers maybe put it in a forum thread instead of just anywhere having it in the database. That would still be as difficult to concatenate, but it just wouldn't be accessible.

Kendall: 00:53:08.920 Depending on how dynamic we want the course to be, we could also ask for feedback halfway through.

Xavier: 00:53:16.579 True. Yeah. You could even go as far as asking for feedback at the end of each module. And maybe a bigger question or a more complete form at the end or something like that. I mean, in any case, I think even if we don't do it too formally, regularly asking if you have ideas, if you have comments, please post them in the forum or linking to a thread or something like that might make sense because the more in context people are when we ask, the more likely they will have something top of mind.

Kendall: 00:54:00.821 Another idea is that we could have them put notes in their like activity log journal thing as they go and note things that they liked or didn't like about each module as they work on that.

Xavier: 00:54:18.809 Yup. That would make sense.
[silence]

Marc: 00:54:51.106 So we ask them to update their journal in module four, five, six, and in the two, of course, when they create it.

Xavier: 00:55:07.971 Well, there should be elements for every week, I guess. Except, maybe, first week, if that's something they do with Git and we want to introduce Git. But they will be working with the project every week. So I think that can easily be included in there.
[silence]

Xavier: 00:55:55.501 I guess also, I don't know if that qualifies for the conclusion. But the biggest we went was kind of module six, fix a bug. So we're already in not something completely trivial. But if we remember kind of the pyramid of the complexity of the contribution that I think Loic was giving, the first level, it's just asking question. Last level is to change the governance of the project. Maybe that's something we can remind here is that like what are the next steps, what are the different type of things they can go after and maybe just try to give them a hint to not go directly for changing the governance of the project, but maybe try to go at that progressively.

Marc: 00:56:53.878 Yeah. So do you want to say to them, "This is not the end," like you did not reach the highest level you can?

Xavier: 00:57:01.559 Yeah. Pretty much. And giving them an idea of the progression and the different steps. I mean, we will have mentioned a bit those steps earlier to tell them to really start with the smallest, but it might be a good moment to remind them, "Okay, now that you're at level three, there's four, five and six, so think about that."

Marc: 00:57:25.530 Yeah. I'm not 100% sure how to introduce it, but that's an idea.

Xavier: 00:57:34.660 Well, I think natural position for that would be when we talk about, "Okay, next steps, don't forget that just like being drive by versus becoming a co-maintainer is something very different. So try to go toward that." Then how, don't try maybe to be core maintainer exactly. So the danger is to get part two of the course all in the conclusion, but just giving them a few pointers as to what the next steps could be like what would be good after fixing a bug, maybe they would want to start looking at developing a feature. Let's start with a small feature, this kind of advice.

Marc: 00:58:26.294 So main advice would be discuss with community what needs work or what's important for the project, so.

Xavier: 00:58:37.034 Completely. Yeah. Of course to choose it. But that's hopefully a reflex they will have gotten from the previous weeks. But the thing is that in communities, people are not always super aware of what a newcomer can do, so also giving them that sense of scale, be useful.
[silence]

Xavier: 00:59:11.770 Maybe also things around how to keep the motivation, how to remain involved. Like things that I think works wells is when you get a regular community involvement, so if there is like a regular meet up or IRC meeting or social discussions that happen within the community or even like, I don't know, yearly conference to go there and to maybe develop even more the social aspect that of course they have started with that, so they will be familiar with the idea. But that aspect deserve to be developed and kind of nurtured over time to also grow. It's not about just asking a stupid question from time to time on IRC. That's great as a first step, but if they want to take more like bigger place in the community, they will also need to level up their social presence.
[silence]

Xavier: 01:00:53.385 Yeah. I think that's pretty much all I could think of there.

Marc: 01:01:12.989 In the OpenStack, Upstream University, do you have like conclusions at the end, or?

Kendall: 01:01:24.455 I think you've covered basically everything that we do. We ask for feedback. We try to give them next steps that they could do. In the context of where we normally have them hosted, the larger OpenStack summit is usually the rest of the week, so we kind of give them a list of sessions that they might be interested in that are more focused toward beginners and the community and getting started sort of stuff, project onboarding and that sort of thing. That's the next logical step after learning the basics of the OpenStack community. So yeah, I think you basically got it all covered.
[silence]

Marc: 01:02:32.549 And, Polyedre, is there anything else that you would want to be covered in the end of the course?

Polyedre: 01:02:39.901 No. I don't have any more ideas about the conclusion.

Marc: 01:02:48.279 It's possible that Remi will join us shortly because he appeared on the chat.

Xavier: 01:03:03.358 All right. So in the meantime, should we try to--?

Marc: 01:03:08.477 To assign people too?

Xavier: 01:03:11.226 Yeah.

Marc: 01:03:15.512 But I think that in any case, the introduction and conclusion are things that has to be done at the end basically after the content, so.

Xavier: 01:03:25.982 Yeah, it will make sense to wait maybe.

Marc: 01:03:30.262 It could make sense to assign people, and it could make sense to wait. I'm 100% sure.

Xavier: 01:03:36.725 Well, especially because a lot of those things might end up being like collective efforts like the, "Who are we?" for example. Because there are some sections that might be more like maybe one person like the grading for example, and we don't get to do all of that. But, yeah, there are few like the advices at the end, it could make sense to have a run like I'm [fitting?] in this. I wouldn't be against waiting for that designation because it's not like if we have a shortage of assigned tasks that we all need to do, so.

Marc: 01:04:19.288 You've not finished all your tasks yet.

Xavier: 01:04:23.576 No, definitely not. I think when we get to have finish everything else and just intro and the conclusion will be nice, but probably we are not yet at that point.
[silence]

Xavier: 01:04:56.774 So then I think we have 15 minutes left. Do we have more to do than the introduction and conclusion? Otherwise I have--

Marc: 01:05:08.506 We can review the whole course and see if we want to add subjects or remove subjects, but.

Xavier: 01:05:17.170 That might be a bit painstaking to do.

Marc: 01:05:21.183 Yes.

Kendall: 01:05:21.816 In the next, yeah, next 15 minutes, that doesn't sound like a thing we should try to accomplish. [laughter]

Xavier: 01:05:28.104 I agree. Maybe with the 15 minutes left, what could be good could be to figure out what will be the next steps because we have been heavily relying on those regular meetings to do things. So like last time we ended up at the end of a series of meeting. There was a big blank afterwards in terms of activity on the project. So it could be great if we could find a way to not have that. So I don't have any specific ideas, but.

Marc: 01:06:04.328 My idea is [inaudible] to--

Xavier: 01:06:05.332 Oh, Remi.

Marc: 01:06:08.532 Hello, Remi.

Xavier: 01:06:08.792 Hello. Can you hear us?

Remi: 01:06:15.234 Yes.

Xavier: 01:06:16.313 All right, welcome.

Remi: 01:06:18.619 Sorry, I was driving, so. And I completely missed the call.

Xavier: 01:06:23.561 Yeah. You arrive right on time, right when we are actually finished with both the intro and the conclusion. So yeah, good timing.

Marc: 01:06:33.441 We are wondering what would be the next to make sure we progress on the course during the next weeks.

Xavier: 01:06:40.431 Yeah because like I was mentioning, we have been relying on the asynchronous meetings a lot. And last time when we had a break in the meetings, we kind of stopped. And I also noticed that a lot of the discussions that happened on the tickets kind of get a bit unanswered and stalled. So, yeah, Marc, you were saying you had an idea to--

Marc: 01:07:07.241 I was mostly first thinking about converting all those chapters into issues to make sure that everything is tracked and people who would want to contribute or to progress on something have a place to tell what they want to do or are doing. Like a specific place. But apart from that, maybe do regular meetings like progress meetings but maybe not weekly.

Kendall: 01:07:42.540 Yeah. I think every other week, or I would say once a month, but that's kind of too far away. So I would say every two or three weeks.

Xavier: 01:07:56.954 Yeah. Every couple of weeks would work for me. I think that's generally a good rhythm. I think one thing that would also really help is if we were able to switch more of the conversations from the synchronous meetings to the asynchronous tickets. So I wonder if we can have some kind of timing for things like when someone is pinged, we could reply within a day or something. I don't know exactly what would be the right approach. But just so that we commit on moving things forward. And at least when we have something to ask from someone, that we know we'll get an answer because otherwise that can really stalls things.

Remi: 01:08:45.789 And we can have a planning also.

Xavier: 01:08:51.999 Sure. You mean for delivering the sections and stuff? Yeah. That could make sense.

Marc: 01:09:06.968 How would you organize the planning, Remi?

Remi: 01:09:15.224 Maybe the person in charge of the week could reach out everybody that is contributing to this module, sorry. And asking how they can commit and estimate duration of how long it will take, something like that.

Xavier: 01:09:44.652 Yeah. Have each person giving a completion date. That could make sense. And where would that information appear on the tickets themselves? Like put a deadline on the tickets?

Remi: 01:10:02.109 Yes. I think yes.

Xavier: 01:10:06.749 Okay. So basically, that would be each person coordinating each module, making sure there is a ticket for each part assigned to the right person, and pinging that person to ask them to set a deadline or something like that.

Remi: 01:10:24.519 Yes.

Marc: 01:10:26.168 Oh, yeah. We can use GitLab deadlines for that, like due dates.

Remi: 01:10:30.898 Due dates, yes.

Marc: 01:10:33.146 And if people assigned can set them, then it's fine.

Xavier: 01:10:39.126 That sounds good.

Remi: 01:10:42.457 Also we can use the milestones. Each module could be a milestone. I don't know.

Marc: 01:10:56.669 No. Not really.

Remi: 01:10:58.659 Maybe not.

Xavier: 01:11:03.898 Yeah. Yeah, because we--

Remi: 01:11:03.946 Basically because a milestone is linked to a date, or.

Marc: 01:11:09.899 It would be on epic, but I don't know I we have epics in the project.

Xavier: 01:11:16.714 Yeah. And also we'll probably want to work in parallel on the different modules, so. But there could still be a concept of release that might not be necessarily module. I don't know. Each person or each module commit on finishing certain sections by a certain date or something. That could be something like that. I don't know if it's useful to do that, but that could be a way to look at that.

Remi: 01:11:57.158 All right. And also the aspect of the minimum viable product to be able to very quickly test with the students. That was an idea we had also, and I think it's a good idea. So every time a part of a module is produced, we can send it to-- I mean integrate it to the MOOC and send it to beta testing.

Xavier: 01:12:31.675 Yep. Makes sense. I don't know how we would go defining because, in a way, even some of the content we already have in some of the modules could already be shown to some students I guess. It depends how polished we want to be. So yeah. I guess the main question is, what do we define as MVP?

Remi: 01:13:08.796 At least when there is some activity to do, it has to be beta tested quite a lot to see if it is working or any blocking part.

Xavier: 01:13:26.429 Yeah, but how do you decide when what you have in front of you is enough to be given to students? Because I assume that the temptation is to have it almost finished by the time you test it, in which case you also have spent a lot of time polishing something that still needs to be heavily changed and become attached to it and etc. So how do you know when you got to the MVP?

Remi: 01:13:59.030 I think when it's integrated and you are able to do the activity.

Xavier: 01:14:10.009 Okay, so when you get to the stage where it's the first version that has been put on the actual Open edX course basically.

Remi: 01:14:17.658 Yes, yes.

Xavier: 01:14:19.638 All right. Well actually, it's true that it brings that topic of the-- like there is Geoffrey who already put in place kind of like the skeleton with the CI building it. So maybe that could be one of the next step is to review that and start maybe pushing some of the content from the brainstorm documents to the course itself. Because there are sections that are very brainstormy and rough, but there are some sections where it's already almost written in a way that talks to the student directly. So yeah. So yeah, there is that pull request that might be a good task to look at next.

Remi: 01:15:11.776 Yes. Yeah, as soon as possible we have to test the whole put-in-production process.

Xavier: 01:15:23.894 So who wants to take care of reviewing that by the way?

Remi: 01:15:34.104 So reviewing this specific pull request and testing?

Xavier: 01:15:38.972 Yeah, to see if we start using that because that should already allow us to have a first skeleton. There won't be actual real content in there. But at least we would have the thing, and we would be able to just, with merging pull requests, to have a course which is the important step.

Remi: 01:16:08.758 Yeah, I see that Khalil said that maybe we should include a documentation, I don't know, but. And there is a link with a GR ticket. Is this your internal--?

Xavier: 01:16:30.679 That's probably just internal, yeah. You can just rely on the-- it should be linked also to the corresponding GitLab, I think.

Remi: 01:16:38.627 All right. All right. So, Marc, you want to try to use that--?

Marc: 01:16:59.586 I'm looking at the merge request. It's uploading to your edX instance, or is it--?

Xavier: 01:17:06.517 That's a good question. I haven't really looked in detail, but I think I remember that it might be using, yeah, like the one that we use.

Marc: 01:17:17.202 It's a variable, so we can--

Xavier: 01:17:19.238 Yeah, yeah. It's not meant to be hard coded anyway, so. Because eventually the idea will also be to be pushing directly to the edX course or wherever we want to have it, so.

Marc: 01:17:32.797 It's also using the numerical long IDs. Is that the same with long IDs?

Xavier: 01:17:44.198 Yeah. I'm guessing that's probably because it's based on a Studio export. But that's typically the kind of things that could be good to discuss as part of the review, if there are things we want to change now.

Marc: 01:18:01.765 I mean, I'm completely for merging and seeing what the CI does with it, basically. [laughter]

Xavier: 01:18:09.136 Sure. I mean, maybe still have a quick look just to make sure that you're fine with [inaudible].

Marc: 01:18:14.168 Yeah. I'm looking at the content. And I think that's good. If it does what I think it does then it's probably good. And then we can discuss details like IDs of documents.

Xavier: 01:18:29.338 Sounds good. Another pull request that would be good to review because that's going to be useful to talk to people, is the draft texts from the project home page that [inaudible] put there. I pinged you on this. I also need to do a review, so I have that in my list. But since it's about presenting the project, it might be good that we are all on board with that. That's like merging it after a week and then, "Haha. Too late. That's the home page."

Marc: 01:19:10.912 We can still edit the page. [laughter]

Xavier: 01:19:13.478 Sure. But you see what I mean? Like especially because right now it's the content, so she will base things on that feedback to actually build the content of the page and etc. So if we wait more, that will create more work.

Marc: 01:19:30.614 Yup. Definitely. I will read this. I will review this too probably tomorrow. And maybe the others depending on the time I have tomorrow.

Remi: 01:19:42.313 So it's the merge request 29?

Marc: 01:19:45.955 Yes.

Remi: 01:19:48.990 Marc, I assign it to you. [inaudible]. [laughter]

Xavier: 01:19:56.515 Good. And the last one on my list. Ah, yeah, there's the business discussion that we need. So that's on your side, Remi, for the bullet point. Do we want to plan for follow-up discussion? And maybe a deadline for reviewing the bullet point we want to send?

Remi: 01:20:21.546 Yes. Let me see.
[silence]

Remi: 01:20:40.448 Wednesday afternoon.

Xavier: 01:20:51.113 Wednesday I am actually off, so.

Remi: 01:20:55.341 I mean I will send it somewhere.

Xavier: 01:20:57.207 Oh, you'll send it? Okay. Oh, I thought you meant for the meeting. [inaudible] meeting, okay. So that sounds good. Yeah, if you do that then I'll try to review. I think I might do it next week because I think that this week will be full. But if need yeah I'll schedule it in any case. And we want to plan for a follow-up meeting, or do we just base it on the asynchronous discussion?

Remi: 01:21:30.388 [inaudible].

Marc: 01:21:30.516 I think every two weeks. Sorry, for the business, or?

Xavier: 01:21:36.182 Yeah, the business discussion.

Remi: 01:21:40.850 Yeah, I think asynchronous is good, and the real big meeting will be with the directors at IMT. Those that have power to decide.

Xavier: 01:21:56.310 Sounds good. And I guess there will also be some follow-up discussion on the edX side. So those discussions will keep moving forward. And we can use the meeting every two weeks to bring it up if something is blocked. Okay, that sounds good. So I guess one last step would be when do we do the follow-up meetings?

Marc: 01:22:22.659 Maybe two weeks.

Kendall: 01:22:23.595 I'd just start them in like two weeks from now.

Xavier: 01:22:27.180 Same time and day? Yeah, that works for me.

Kendall: 01:22:31.880 Yeah. Do we think we need the 90 minutes, or is an hour enough for check ins? I feel like that's plenty.

Xavier: 01:22:38.951 Yeah, I would vote for one hour too.

Kendall: 01:22:41.910 Okay.

Xavier: 01:22:45.745 I will not change that actually.

Marc: 01:22:49.685 I think I have a recurring meeting in my calendar anyway, so.

Xavier: 01:22:54.755 Yes, that's one that I have sent, so I am actually going to change it biweekly. I'll do that right after.

Remi: 01:23:03.696 All right.

Xavier: 01:23:06.306 All right. Perfect.

Marc: 01:23:09.436 So it's on the 7th, right?

Xavier: 01:23:14.177 It's what? Sorry.

Marc: 01:23:15.206 June 7, right?

Xavier: 01:23:18.156 The next one will be, yes, on June 7. And I have updated the invites, so now it should appear once every two weeks. And the next one is June 7.

Marc: 01:23:32.250 Yep.

Kendall: 01:23:33.671 Cool.

Xavier: 01:23:34.666 Perfect. All right. So that's great. Well we did a lot of work with all of that by the way. It's going to be a lot of work to finish formatting it. But yeah, it's pretty neat.

Marc: 01:23:47.225 Yeah. Yeah, thank you a lot everyone to contribute to this. It's taking a good shape I think.

Xavier: 01:23:55.818 Yeah I agree. I'm really impatient to see the final form. I think it's going to be really nice. And it's definitely one of my favorite part of my week when I get to work on the MOOC. So it's really cool. All right. Well, have a good evening or end of your day and see you in two weeks then.

Marc: 01:24:20.940 Have a good two weeks.

Kendall: 01:24:22.718 Thank you.

Remi: 01:24:22.743 [inaudible] thank you.

Marc: 01:24:23.793 And see you soon on GitLab anyway.

Remi: 01:24:25.982 Yes.

Xavier: 01:24:26.430 Yep. See you. Ciao ciao.

Remi: 01:24:28.619 Thanks all. Bye-bye.
