# Brainstorm - Module 3 - Meeting 2 Transcript

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1618851298188

S1: 00:00:01.877 Okay, let's go. So, Xavier, I saw that you transferred the brainstorming session of module three into proposal for structure for the course. So can you tell us more please?

S2: 00:00:21.907 Oh, sure. So let me see. Actually, I could maybe share my screen and get that merge request on the screen. That might be the best. I don't think I've ever shared my-- oh! I got the right to share my screen. That's why I could never see it. Let's see. That's not this one. That's this one. All right, so, yeah. So for week three-- so I took it, and basically I haven't really done any editing, really, on this one. It's like everything that was mentioned is in there, so you might want to sort out some of the points. But I've tried to get structure from it to regroup because especially the conversation was all over the place, so-- but there was also a lot of really good ideas.

S2: 00:01:28.618 So basically what I did is that the structure is around-- one of the main points that was mentioned is the social nature of free software. So that the fact that you need to talk to people and figure out who people are to be able to do things. Then we talked a lot about definitions, like what's a project versus an organization, the different types of actors or stakeholders that would be associated with the project like difference between communities, companies, organizations, etc. So I think those are useful concept. I don't know necessarily about the order that they want to do because we might want to make things interesting, so maybe some activities might be good in there, but at least that was a point that was coming back a lot.

S2: 00:02:23.079 In terms of activity around that, there was a few suggestions, so I've put them there. We talked a lot also about licenses and economic models. So the content itself, I don't think we did too much, so that's why [inaudible] [are few and sparse?], but at least I think we have the main ideas. I think we talked more about economic models in terms of-- especially the influence and the relationship that they have with licensing. The way that also impacts the potential contribution that we want to do to make sure that we are in the [inaudible] with the licenses and business model. Otherwise things don't happen.

S2: 00:03:15.603 And there was also a lot of discussions around activities on that, including even some preparation for the activities, because we were talking about whether trolls could be an issues. But the activities themselves, so there were lots of ideas around case studies, so presenting a bit the different configurations of communities or projects, and etc., and trying to get the students to recognize the configuration, maybe guess what type of contribution would be good for a given scenario and also showing that there is rarely just one answer and just trying to show the diversity of approach. And there was also another type of activity that came from [Luke?] around reading and explaining license. So it's trying to demystify a bit the license text itself by getting student to explain it, to maybe find questions that would correspond to Creative Commons questions around that, like a few ways to get the students to actually read the license and understand them. Then--

S1: 00:04:41.118 This one can be very hard, right?

S2: 00:04:44.125 Yeah. It can be pretty hard because in a lot of cases, we've said what we wanted to do, but not exactly how either. And we haven't really broken it down into actual steps. But, yeah, I mean, the one that [Luke?] was mentioning, he was mentioning that he was surprised that it was working quite well, that students were actually reading and interacting a lot on that, but I think he had the advantage of having face-to-face interactions, so they kind of had no choice and had to actually read one line and think about it under the social pressure of their peer listening to them. So I think that's what probably helped to break that thing of, "I don't want to read the license." But apparently once he got that, that was working. So yeah, I guess the point that we probably haven't solved is how we replicate or transform that in our context. So might be a point of discussion.

S2: 00:05:51.847 Then also the project tips or kind of the recurring last section of each module. So I put a bunch of points, the points that came from that week, but what I'm realizing doing those things is that each week, we talked about a lot of things with that. But it might be worth at some point, especially once we have a few weeks, trying to rework that to where the context of the whole course, differentiating what's in module one, module two, module three to really organize it well because I think some of the stuff might be worth changing places. But still there is, I think, a good structure.

S2: 00:06:39.800 We talked about some of the negative aspects of contribution this week, like there was trolls before, the fact that it needs too much remodel, but also rejection because we were talking about potentially contributing to Wikipedia or Wikidata, and that's a place where there are lots of reverts. So it would be potentially a good context to mention that. One of the reasons I was mentioning that we'll need to probably shift some stuff within the module is that one of the ideas we had while talking about that is that before getting people to contribute within the context here, where they would try to update Wikipedia, Wikidata, to add details about specific projects like who are the actors? What are the business model, etc.? That it might be good to get them to do a contribution to Wikipedia at all, maybe in a previous week. And then probably if we do that, they will still deal with the reverts, so maybe typically dealing with rejection might come in an earlier week as part of that. And here we would talk more specifically about that contribution.

S2: 00:07:53.549 But anyways, I've still kept things in their own module for now, and we'll see how that goes. So yeah, that's the main activity around here is basically-- again, often the project [inaudible] kind of an echo of one of the activities during the week. So I think in this case, it [inaudible] the case studies of trying to figure out what's the licenses and business model and actors and etc. are in the context of some examples that we already know and that are the same for the student. And the later projective activity is about the context of their [inaudible] which is much more open, but at the same time, we keep every time the step small as possible by getting them to do something that they just did, but on their own project. So that was that. And I think the end is just to do some of the points that I mentioned but that I couldn't immediately find a good section for. But yeah. That's the general structure that I could see in there, but obviously, the idea will be to rework that a little bit. What do you think of the structure that I [inaudible]? Does that match what you had in mind?

S1: 00:09:39.806 Yeah, most of the points match what I had in mind, but maybe we can think about how to engage students more with activities mostly and see what will be the main pain points and how to insist on what we want to convey in this course. So the main point of this module is, collaboration is not something you do alone basically, by definition.

S2: 00:10:14.976 Yeah. I think the main point is actually probably that first section, the social nature of [us?]. I mean, I don't know if currently that's what you have in mind, but I think it's definitely worth starting strongly on that one. I mean, it's going to be reinforcing things from the previous weeks, actually, because even from the very first week, we want them to go say "Hi" and stuff. But here I think we are really digging into that, yeah.

S1: 00:10:45.323 Maybe that's worth also insisting at the end of the module, probably a recap that just insists that all of those things we've seen in the module, like licenses, like discussions, [talks?] and-- basically everything of this week is caused by the fact that we are a bunch of humans trying to interact with a common goal.

S2: 00:11:24.030 It's true. And especially because it might not be completely upfront for some sort of like licenses or maybe in some cases, business model. I mean, in retrospect, it is obvious because those are mechanism of collaboration, actually, but often, the thing is like, "License is dry. It's just for the lawyers," and etc. And no, actually, especially in the free software, it's actually written and designed in a way that encouraged that, and that's also why we want to read it. It's not like terms of service which are basically just there to be good for the company that makes you sign it. Those are really made to be real terms of collaboration, so. Good point.

S1: 00:12:06.880 Yeah. It's basically a list of things that you are allowing people to do with your code or allowed to do with code that others wrote.

S2: 00:12:16.169 Exactly. And actually, that's also something that echoes maybe some of the issues around business models and the recent kind of issues with Kibana and Elasticsearch and etc. It's maybe companies who haven't really read correctly the license at the beginning and realize, "Wait a minute, did we agree to that? No, let's change the terms now." So yeah. That's also a big point is that once you've committed to one, then you need to follow through.

S1: 00:12:48.593 Yeah. Yeah. I don't want to start [inaudible]--

S2: 00:12:54.792 No, no, no. No, no.

S1: 00:12:56.438 Maybe they didn't plan on Amazon becoming what they became.

S2: 00:13:01.606 Of course. No, no. And especially I've been defending the point of view that we need to have different points of views. And there is a part of that that I personally am in favor. I'm like very AGPL [inaudible]. And in a way, some of what has been done-- I don't remember. I think it was the first license. Their main argument is that they were trying to extend the meaning of the AGPL so that someone can't proprietize their service on Amazon. Which, I mean, I don't know exactly where the line is for OSI, for example, but there is definitely an argument. So I don't think we should conclude on that. But what I think is that it illustrates that point, the fact that, once you've chosen a license, if you want to change it, it's going to be really complicated. You might even turn your community against you or something like that.
[silence]

S1: 00:14:20.436 So problem with starting with activities is that-- or activities being mostly case studies, it's hard to do them after-- well, it's hard to do them before having explained what existing things are, so.

S2: 00:14:38.332 That is true. Although, there was one idea-- it might still not be a way to do it before, but there was the idea of using the case studies to teach which is to have some of those elements that would be like, "Intuitively, what would you want to do in a case like that?" And maybe some of the case study would already be explaining, "Okay. In a concrete case, there was this company. We chose that license and etc. for that reason," and then have a part of that being like, "Okay. What should be the business model?" So you still have to explain some stuff before. It can't be really the very beginning of the story and the module, but it could still be part of the teaching itself other than being something strictly at the end. But it's true that it would be nice to start maybe with something a little bit more interactive.

S1: 00:15:46.438 Maybe after explaining the social nature of FLOSS like introducing it, we could start with trolling.

S2: 00:16:02.936 Oh, you would go directly in trolling? All right.

S1: 00:16:07.285 Something like, FLOSS people interacting. And one of the most common things in interact is discussions. And sometimes it's-- not trolling, but saying that debate is a fundamental part of interaction. And people do not always have the same premises and the same point of views. And so it generates discussions. And we can say that most heated discussions are about things that people are fundamentally-- things on which people fundamentally disagree on like economic models, for instance. And people can see that-- like some people see that fundamentally, there is the role of the state to provide for most basic things and things-- some people fundamentally [prove?] things that the private sector is more efficient and that the state should be minimal. And that has impacts on how they contribute and how they tend to see their economic models, for instance. And that translates into licenses and etc.

S2: 00:17:35.680 Yeah. That's an interesting way to approach that. Yeah. I think that's good idea honestly. Especially because, yeah, we can really use that to show from the beginning that it's not a clear cut thing, and that we are going to present a different point of views and etc. I like that. The only thing is, you still don't have an activity in there though.

S1: 00:18:11.723 Yeah. I don't want to ask people to troll. It's too difficult.

S2: 00:18:16.551 Yes. Yeah. It's difficult. And yeah. I mean, they will probably do it anywhere in the next sections, but yeah. Because in the activities that we've seen, I don't see many-- I mean, like the reading the license, I think that must be a bit violent to start to the week in any case. Just like we said, we need to have some work on-- I mean, on trolls, you could always recognize the troll. Some sample from threads or something, maybe even some that are already about license or business models. So it could be a nice way to present some point of views, using - I don't know - maybe famous threads if there are some or something like that then and why is the troll. And probably people will actually disagree depending on their point of view, actually.

S1: 00:19:53.602 Yeah. Might be a bit dangerous to do with concrete examples of discussions.

S2: 00:20:03.812 Yeah. I mean, again, depends what you consider what the danger is. But we don't have to give a definitive answer. One of the approach, I think it was, with the case studies, where we have similar mind field when we ask people to pick, what's the right license for this context or whatever. Of course, people are going to disagree with that. But what we can do is instead of giving a definite answer, we can give, "Okay, here are the stats of what people replied in of course. Here is what actually really happened for that project. They chose that." Or, "In that thread, here was the conclusion afterwards. People decided to do that." And so, of course, the choice of the examples might have a little bit of ideology with it. But if we have different ones, and we just let people compare their notes on that, that might be enough. Especially if one of the main points of that section is to say, "By the way, there is no definite answer." And the principal of trying to avoid the troll is to not fall for the replying rage and the debate, but trying to see the different point of views.

S1: 00:21:20.376 You mean that the goal of this module is not to say that AGPL is the best and--?

S2: 00:21:27.279 I'll still manage to put that somewhere, but it has to be subtle [laughter].

S1: 00:21:45.218 Main issue with starting with troll is that it's a bit hard to go back to a serious subject after that, but.

S2: 00:21:57.458 Yeah. I mean, it depends because the part of the approach that we suggested for the trolls was talking from a sociological and anthropological point of view. So yeah, maybe the activity might have a little bit of fun, but I don't know. I guess it depends on how you approach it. But I would not be too worried about that, though.

S1: 00:22:25.749 Okay. Well, then probably license and economic models, and then as an application, reading the license? That's--

S2: 00:22:43.488 Yeah. I mean, that activity, I'm still not sure how we do it concretely in a MOOC context.

S1: 00:22:57.586 We can [inaudible] have multiple choice questions on a specific license.

S2: 00:23:05.185 But what would be the choices?

S1: 00:23:08.013 Like, can I do this? Yes/no. If I edit code, can I distribute it? Can people ask to see my contributions?

S2: 00:23:26.297 Yep. [inaudible].

S3: 00:23:28.046 Yeah. Maybe a question might be, "What is the best license for this kind of project and why?" So maybe there is one answer for this question, but explaining why a license is relevant for some type of project, it may be be useful to force the learner to understand deeply a license and the goal maybe,

S1: 00:24:01.957 How do you check that they understood?

S2: 00:24:06.077 Well, I think that for that, the case studies approach is good. I agree with you, [inaudible], that that's probably a more practical thing because the MCQs, I think it's going to be very dry. If it's like, "Go read that license and then check which of those things." I mean, I would do it as a learner, but I would not be super enthusiastic about it, to be honest. The case studies, again, so that's what was described here. Give real examples and give the constraints, for example, of the project. It's a company who wants to make money on something, and they want to have an online service with it or something. You give some of the elements maybe of the business model, of the licensing choices or their context, and get students to figure out, okay, what are the possible licenses if that's their constraints? And, again, without the approach of giving a definitive answer, we can say what the product actually did in that case, but that's one answer, and also show what those students were thinking was potentially the best choice in that case. And I think it helps to understand that because then you end up being in the perspective of-- taking the perspective of the project, having a bunch of licenses in front of you, and you have to pick one. And you also take the perspective of instead of just being the contributor, which will be the case of the learners here, you're also, "Okay. Why would the project pick that license from their perspective?" So, yeah.

S1: 00:26:03.671 Again, so I have what the project team thought would be the answer.

S2: 00:26:09.163 Yes, pretty much.

S1: 00:26:11.502 Or what would experts on the subject, if we interview experts on licenses, which I think we should.

S2: 00:26:18.852 Yeah. There is a section about that. I think it's here in the part-- yeah. It's when we talk about licenses. Yeah. Here we go. Interviews. And, again, we can also show that you have diversity of perspective by having someone from opensource, someone from free software, even - I don't know - someone from Elasticsearch or Amazon, even, who knows, we can put some trolls in there too, so [laughter].

S1: 00:26:56.910 I wouldn't interview people from Amazon.

S2: 00:27:00.129 Well, I mean, it's a discussion we'll have to complete because we have that in a lot of areas because there will be a lot of people with whom we disagree, right? And the question came on [inaudible] and also earlier [inaudible] was a little bit of a pain the ass, but if we exclude people we don't like, I think, necessarily, that's going to make the course ideological. And I think, at least for me, the reason I'd like to have both the opinions of people I agree and the people I disagree with is because, at the end, especially given that social nature of contribution, what counts is not necessarily what is my opinion. Am I right? Am I wrong? Is the person in front of me right or wrong? It's, can I see their perspective, and can I find a middle way between my way of doing things and their way of doing things so that we are able to work together?

S2: 00:28:01.899 And I think that necessarily goes through working with people we disagree with sometimes and being able to say, "Okay. You said that. I disagree with that. But can we do that at least?" And if we can pass that, I think they will be a lot more equipped to get their contribution merged at the end. Because, for example, for the case of Amazon, I'm not a huge fan of them either. But imagine you want to contribute to Elasticsearch. Do you go to Elasticsearch? Do you go to Amazon's fork? If you don't know their point of view, it's kind of hard to decide. And I don't know if we can decide for them, right?

S1: 00:28:55.933 I would probably go to the Amazon fork.

S2: 00:28:59.732 See, that's really tricky one, so.

S1: 00:29:12.424 So do we do a [inaudible] we had?

S2: 00:29:20.263 Oh, right. You have the URL? Thanks.
[silence]

S1: 00:29:56.359 You want me to copy some of those sections even if we move them around?

S2: 00:30:02.576 Just to make sure we don't forget anything when we discuss what can be afterwards.

S1: 00:30:10.988 Okay. I'll start from the bottom, and you start from the top. I'll get there.
[silence]

S2: 00:31:15.267 You're already on project tips. Okay, so I just need to delete those. How do we delete-- ah, yeah. Okay.

S1: 00:31:42.504 Yeah, it's hard to contribute. We need communication.

S2: 00:31:46.025 Yeah, we got project tips. So then--
[silence]

S1: 00:32:43.984 I would put the types of actors before the project versus organizations because it's easy to understand the different types of actors, but it's harder to differentiate.

S2: 00:32:59.661 All right. Let me see. Types of actors. Sure, yeah, that sounds good.

S1: 00:33:11.562 So after introducing the different types of actors, we can say that these actors are not the projects. They are actors in the project, and there can be lots of actors in one project and lots of projects for one actor.

S2: 00:33:25.843 Yep, that works for me.
[silence]

S1: 00:34:02.396 So you put case studies and reading [and explaining the?] license near one another because it's [crosstalk]?

S2: 00:34:08.935 No, I've just replicated what's in the document right now. It's just like a [wall?] section about activities, but there was no real choice about which one or where to do it. It's more that they are related to license and economic models, so that's why they are in that section. But yeah, what should we do in terms of the--
[silence]

S2: 00:34:47.856 Actually, we talked about trolls and disputes. Think like the case study thing can probably be-- we'll probably want to have several case studies, right? So maybe that could come in between sections within the discussion of license and economic model? Maybe it start with the-- If we start with licenses, first case study, that deal mostly about license choice or something, maybe on a project where there is no economic model yet or something like that. And then later on, we have a second case study which shows more an economic model particularly and how it links with licensing. See, like breaking it down, not having just like one big activity with 10 case studies, but some of them spread out.

S1: 00:35:49.195 Yeah.
[silence]

S1: 00:36:16.468 So it could be that-- actually, we could have case studies being an activity and have that being duplicated like two or three times.
[silence]

S1: 00:37:03.353 Okay, and then I'll delete it from here. That's [inaudible]. Limiting the activity of collecting all the information we've discussed for your project [inaudible] choose to contribute in and putting this in [crosstalk].

S2: 00:37:27.680 That's in Project Tips. That's collecting information about the project's actors and contributing the information to Wikipedia and Wikidata. That's the main thing that Project Tips is about is that week.

S1: 00:37:44.941 So it's one-- okay.

S2: 00:37:49.522 Yeah, so yeah. The second two activities-- yeah, those two activity and the first one is, I think, text or videos.

S1: 00:38:07.399 Or interviews.

S2: 00:38:09.679 Yeah, we'll probably spread out interviews through the course. But yeah, that's definitely a good areas because then I'm sure people will have anecdotes with that. And again, having someone known explaining how they had to deal with that I think is reassuring, so. And again, some of that, like I was mentioning earlier on the project tips, some of it might have to shift between weeks. Especially if we do our first Wikipedia contribution a bit earlier, then that rejection section might move there. But for now, we can just leave it there.

S1: 00:38:54.023 Yeah. We have contributed this information to Wikipedia. Didn't we have something like simply fix a typo on Wikipedia or something like that?

S2: 00:39:09.864 Yeah, that's what I'm referring to is that we talked about contributing to Wikipedia for the first time in this module. But while we were talking about it, we were like, wait a minute, maybe updating a project and etc., something people are not familiar with might not be the easiest first contribution. So it might be good to get them to contribute anything to Wikipedia first, like something on topics they are an expert of, or maybe a typo or something. The thing is that it would probably be something in an earlier week or potentially a second section in that week, but it is mentioned I think--

S1: 00:39:51.704 It is also likely that it's something that will be reverted or?

S2: 00:39:56.542 Yes, exactly. So that's why I was saying that in that case, the rejection should be moved to that week if that's the case because then that-- I don't remember why I put these-- yeah, yeah, there you go. So that's a to-do item on the Project Tips activity.

S1: 00:40:27.243 Do we have everything? I'm not sure.

S2: 00:40:32.252 I think, at least, from the bottom, I think we got all the sections. [Surely?] this, we can probably move it here, [inaudible] activity here. Yeah, I think reading and explaining a license, we still don't really know what we would do in there. Because there is still the possibility, like you said, simply you ask people to read the license and have an MCQ, but I think we lose a bit what [Luke?] was explaining which was the interaction, the fact that people would explain to each other all kinds of rubber duck explaining what that line of the license is. And I don't know exactly how we can replicate that.

S1: 00:41:47.516 I don't think there are ways to replicate that in a massive environment.

S2: 00:41:53.726 Well, the thing that comes to mind in terms of explaining to each other, like peer review, is that you could always either have a text or maybe even a video or a note or something - people could choose what they prefer - where there is-- we need to kind of split the content of the license or something and each person has to re-explain it. I don't think that's going to give something great though.

S1: 00:42:26.390 But it won't be interactive, right? People just do contents for others and review contents from the one before and-

S2: 00:42:35.522 Yeah. I mean. You still get feedback from someone else who listened to what you said, so you do have a little bit of interaction, but it's very asynchronous. So you lose that, "Oh, that person is listening to me, so quick, quick, I need to explain that." [crosstalk] that.

S1: 00:42:50.527 Yeah. But you also lose feedback, the opportunity to change your explanations based on people say that you are wrong and--

S2: 00:43:01.245 Well, it's true. You just get the review itself. So, I mean, still, the goal of peer review is that you get that feedback from someone else, and you realize, "Oh. Okay. Yeah, that might've been wrong." But it's true you don't get to actually edit that answer or something afterwards. And it might get to a bit complicated to do it like that too. One of--

S1: 00:43:33.068 If we take like three licenses, for instance, so GPL, MIT, BSD, can we have some-- can we make sure that if someone is tasked to explain MIT, for instance, they get to review one GPL and one BSD? And--

S2: 00:43:55.604 Not with the standard tools that we have right now. You don't really even have a way to take a text and split it randomly between students just with the peer grading tools. So if we wanted to do something, we would have to code something specific. So that might be a bit big for what we want to do here, I think. Another option that has been suggested was to simply have students organize in groups, maybe in the forums, and different groups would be taking different license. Maybe the students who are not very interested might even skip that activity because we don't really have a close way of controlling that, I guess. Maybe a link to the--

S1: 00:44:47.855 Does it work?

S2: 00:44:50.565 Well, I mean, [crosstalk]--

S1: 00:44:51.909 If you ask [crosstalk] to self-organize and make small groups, does it work? Or is there an effect of, "Oh. There is a big group. I join that and see"?

S2: 00:45:03.911 Well, it depends. Definitely depends on the number of students in the course. If we do something that is really purely self-organized in the forum, there might be one thread per license and etc., and people might just not have anything left to do by the time they get to that thread, potentially. But I could see a version of that that could potentially work where on each of those threads, there is the text of the license, and each reply is supposed to be taking the next line of the license that hasn't been taken by the thread above. It could be worth testing because it's very low cost for us to try. And we see if that works.

S1: 00:45:56.253 Yeah. But that does not work with parallelism.

S2: 00:46:01.812 What do you mean with parallelism?

S1: 00:46:05.367 If there are few students at the same time, then they will comment on the same line basically at the same time.

S2: 00:46:15.933 True. Is it a problem though? Because they might have slightly different interpretation?

S1: 00:46:22.836 I don't know.

S2: 00:46:24.584 I mean, that's one option. The other option is to use actually the-- I don't remember the exact terminology for that in [inaudible], but there is a way to have groups that are dedicated. So it has a little bit of structure for self-organization into smaller groups, and each group could be picking the license, and they could be able to keep it small enough at that point to have something a little bit more interactive, I guess. So--

S1: 00:47:00.244 That would be nice, if we can make group small groups of students, and then they can explain one license, and.

S2: 00:47:09.464 That could be actually a way to try. I mean, we can [inaudible] again, many parts of the course, see what works with that. And if it doesn't work on one iteration of the course, for the next one, we'll just find something else.

S3: 00:47:27.702 I have a question. Last week, we talked about having a GitLab project so that students can try and contribute and do exercise about Git. So this is not relevant here? We cannot use it?

S2: 00:47:54.448 That's an interesting idea because it's true that we could have, for example other licenses as merge request, and people comment on it, something like that? Yes, we would still have that parallel. It kind of gets to the forums, right, the example that I was giving about, because multiple people could be commenting on the same line at the same time. Not even sure GitLab allows that because you're supposed to only have one thread per line. So what happens if two people add one at a time? I don't know, actually.

S3: 00:48:28.756 Probably [inaudible].

S2: 00:48:32.596 Hopefully, that could be the case if the [inaudible]. Was that something like that, that you had in mind or something else?

S3: 00:48:39.615 Or maybe just having students create a merge request to their GitLab with an explanation of [inaudible] license, for instance, and having those groups made so that they have to review each other. [inaudible].

S2: 00:48:59.462 Yeah. You're right. That might actually be better. And it gets them to practice the tools too. That might be a good one. So the explanation would be mainly in the reviews, right? Would there be a way to have something produced off of that?

S1: 00:49:26.555 Do we want something produced or?

S2: 00:49:28.984 I don't know.

S1: 00:49:31.176 Because I think--

S2: 00:49:31.433 Maybe not.

S1: 00:49:32.962 --already good explanations of the free software licenses and--

S2: 00:49:38.599 Sure. Yeah. Maybe not. So also maybe to control that it's done. How exactly we do that? I mean, we can always just check that-- they enter a URL of a merge request, and we check that their accounts have commented there or something.

S1: 00:50:00.578 Yeah. We can check there are comments from the other of the groups?

S2: 00:50:07.586 Something like that. I mean, it's custom code, so yeah. But still, that's a possibility, at least.

S1: 00:50:14.279 What's supposed to happen in groups if some of the groups are inactive? Or should we make the groups big enough so that they're always a minimum?

S2: 00:50:25.217 I haven't really used that feature, so I don't know exactly what the limitations and possibilities of it are. I think you don't have a lot of control over it. I think it's mostly, I think students, and maybe staff can create the groups, and then whoever joins whatever. And I don't think you have much more control beyond that, at least from what I remembered. But we might want to test a bit and see what exactly we can do with that.
[silence]

S2: 00:51:19.054 So for the small groups--

S1: 00:51:20.348 [crosstalk]--

S2: 00:51:21.590 Yeah. Exactly. Yeah. I think so. Probably same thing for economic models. All right. Have we covered everything we wanted there?

S1: 00:51:56.892 So graded activity is a quiz, right? That's what makes the most sense, given it's mostly information [inaudible]?

S2: 00:52:13.841 Well, there is that. But I mean, again, that would require some code, but-- well. it's true that grading-- I mean, it depends how much fine grading you want, because part of the grade can just be, has it been done or not. See this section around the contribution? I think it was quickly mentioned as something that could be grade or at least validated through peer review, where we would use the information that would be gathered on the project, either in Wikipedia or Wikidata. We would find a way to represent that and get the learners to provide the URL for this either to the recent change of the page or to whatever the equivalent for Wikidata is, and then have that peer reviewed. So that there is the upstream review of the contribution, but there's also kind of an internal course review from others, and as a way to provide more feedback also like, "Did you look at the right thing? Did you do the correct thing?"

S2: 00:53:33.305 So I don't know because one of the other things that we talked about in the meantime - I think it was for the last module - is that peer review can be a bit heavy. And we also talked about the journal of contribution or at least getting to a contribution on that project, and that these might be the thing that might be peer reviewed. So this might end up being one of the elements that get reviewed more broadly over several weeks by a peer review. So that there are few pending questions, but at least we talked about peer review as a way to provide some of the grading including on what we would do that week.

S1: 00:54:18.051 I don't think it would be a good idea to grade the fact that they contributed to Wikidata because it's a task that will get more difficult over time. The first one will see, "Oh, the Wikidata page for Linux doesn't have the license set in it, so I will just add it." And the 100th student will say, "Okay, all the most obvious information has been added, and I can find something," and maybe the 1000th students will be, "Okay. So all this I checked, all this I checked, and I'm a bit tired now because everything that I can think of has been added."

S2: 00:55:07.766 It's true that it might be a concern for the more popular project where a lot of students would be choosing the same one. One of the things we said during that brainstorming is that we would probably never be able to get to a point where this is completely done for everything because there is always information that changed and etc. But it's true that for projects which would be chosen by a lot of different people, yeah, that would be-- yeah. I mean, if this is one of the elements that is graded as part of the journal of contribution, then it's less of an issue because it's one of the elements. And if there was nothing new, well, then I don't know the criteria for that part, I included an option for, yeah, that's a big project, so everything was already up to date or something like that. And it is not too much of a big deal because it's one of many things they would have to do anyway. But yeah, it depends how we handle that exactly.
[silence]

S2: 00:56:37.848 Yeah. I think one of the points that was mentioned during that was also to not leave learners alone in front of Wikipedia and Wikidata. And that the peer review was a way to get some feedback, to not be completely alone. Maybe there are other ways to do that - I don't know - through interactions on the forums or something else or something around the journal again, because I don't know. Well, people will have mentors if we implement that. We haven't discussed that much. They will not be left completely [alone?]. But for the other ones, having mechanism-- and I think that's going to be a recurring theme on their wall journal of contribution. We might want to pair maybe some students with each other so that they can provide each other feedback or something, maybe have groups around that. Maybe people will join to contribute on a specific project. I don't know exactly.

S2: 00:57:50.329 Actually, I haven't thought about that a lot, but having people try to contribute, be several people choosing the same project and trying to approach the discovery of that project together might be nice because they might have to collaborate with each other there. It would solve that aspect of not being alone. I mean, it will be several clueless people, so it's not necessarily perfect either, but at least being clueless with other people, it feels less lonely, I guess. And they might find more things. Some people might get an answer. So yeah, for those who don't have mentors especially, that might be useful.

S1: 00:58:34.397 But should we leave the choice of the project completely free then, or?

S2: 00:58:40.588 I mean, I think it's good to leave it completely free. I don't think we want or even can prevent people from picking their own project at the end. But especially in this choice of the project, that might be one of the options we provide. Some people will already come, they know already which project, whatever, and then go, "All right, no problem." But people who are not sure, we've talked about suggesting some project, but also maybe even the ones who know, if they say, "Okay, I want to contribute on this project," if they have to still create - I don't know - a little group where they say, "Okay, my project is - I don't know - Inky or Inkscape or whatever," then other people we don't know they might be encouraged to see-- could see if there is a project where there is already someone doing that, and even the person who chose their project from the beginning might appreciate the company, so.

S1: 00:59:39.477 We can give to the students a list of projects that has been taken by between one and five people so that we don't have groups of more than six, and it creates group by showing, "Oh, this is a project that exists and has been chosen by at least one person."

S2: 01:00:02.407 I don't know how much capability we have or is existing for that, but I think we can at least give it as an instruction. Because I think those groups, at least if we used the feature from Open edX, the thing is kind of freely creatable, and then I think you see the number of people that are in each small group so we can say like, "Pick one which doesn't have more than five," or whatever. I don't know if we can set a limit or something. We can also say if there's already already been five people, create a new one. And then other people will join that one. And I guess these are also things that potentially we can automate in iterations as we see how that works, and we're like, "Okay, students definitely get tripped by that." Then we try to make it a bit better or to automate and, yeah, add a feature.

S1: 01:00:58.434 Is it the team switcher from Open edX?

S2: 01:01:01.575 I think so. Again, I don't remember the name of it, but that sounds right.

S1: 01:01:08.367 You can set a max team size.

S2: 01:01:11.683 Oh, perfect.

S1: 01:01:13.358 Which is the only thing you can set.

S2: 01:01:19.038 Yeah, I don't know who developed this, but it's interesting that it's one of the first things we want to set. At least they got that right.

S1: 01:01:31.517 I wonder if they can join multiple teams which we put not--

S2: 01:01:41.583 Although what I see here is that the teams are created by the staff, but I thought there was a way for learners to create teams. Because if we have to create all the teams, that's going to be a problem. Oh, no, you can create teams in a topic, so actually, that should be fine. Post team member [inaudible] the staff admin discussion, [inaudible] discussion whether it all can-- new teams and-- ah, shit. Needs to be a staff person.

S1: 01:02:19.695 Maybe it can be a script.

S2: 01:02:22.589 Could be a script or we could contribute a feature that allows anyone to create a team which might be a good idea as well because I don't know why that's not possible. Anyway, we can look at that more precisely when we work on this, but I don't know why I haven't thought of that option earlier because that could be a nice thing to have other people [around?] contributions. And then actually that might have names like what we have discussed earlier pick a license. Then you just pick the license of your already existing chosen project with your co-buddies from that project. I mean, it depends on how much people-- if we have too many people who are alone, that won't work, or if we have people dropping in the middle of the course, that might be a bit tricky too. I guess we'll need to see how that works in real life, but.

S1: 01:03:24.750 I think there are always people dropping.

S2: 01:03:27.479 Yeah. Yeah, but to the point where that would be an issue because it could be like people who didn't really engage drop quickly. But they are not a problem because they don't-- I don't know exactly. I will need to see. I think probably for the first iterations, especially the very first one, we might want to use the options that give us the most flexibility to handle things we haven't completely thought of. So typically, with the teams, it looks like a nice feature, but at the same time, if we have people dropping, then suddenly we can have teams where there is only one person, but then we don't really have a way to merge those teams or whatever. So maybe forums in those cases, at least for the first one might be better, especially if we use something like this course, that has a bit more flexibility on the moderation side. I don't know. That might be a good idea to prototype things before we switch to more hard-coded features.

S1: 01:04:37.199 I find the interface of this course very, very confusing.

S2: 01:04:42.148 Do you? I mean--

S1: 01:04:44.450 It was very far from forums that I was used to.

S2: 01:04:49.999 Oh, yeah. But then--

S1: 01:04:52.138 It was more like mailing list with tags.

S2: 01:04:56.328 Well, yeah, I guess maybe you're referring to the fact that you have topics instead of having subforums. So yeah, it's more like one big feed with everything in [inaudible]. That is true. But in practice, it's rarely an issue. Most people don't even choose the topics correctly most of the time. One of the main thing I do with moderation is retag the categories. So they don't care. They see, "Post here," and they just go for that. So in practice, I think it works fine as long as you're willing to do the re-categorization work, which might be less of an issue. I think, even with traditional forums, people just post anywhere, so. So yeah, I think it works, yeah.

S1: 01:05:47.428 I'm used to phpBB forums.

S2: 01:05:50.757 Yeah. But you're really an old-timer there because that is kind of terrible. I mean, I've used it a lot too, but oh, my, that's--

S1: 01:06:02.047 For video games, sorry.

S2: 01:06:04.595 Yeah, yeah. But I mean, I'm surprised because most of the communities I've switched to discuss [inaudible], it's really old stuff like [inaudible] existing big-- but now you have migration script from phpBB. So, yeah. I don't know.

S1: 01:06:22.215 I haven't been in those forums for a while.

S2: 01:06:26.355 Next time we [inaudible] them, you might be, "Damn it, [this is this course?] now," so.

S1: 01:06:31.627 If people like it and it's good.

S2: 01:06:35.366 Yeah. I think at the end, it has a lot of usability improvements, actually, compared to the-- the interface is a bit disconcerting at first. It's like it looks a bit clunky and etc. But when you use it, you realize that actually [inaudible] a lot of stuff is true.

S1: 01:06:54.001 Then maybe we can have a discourse for the course, yeah. Discourse for the course. [laughter]

S2: 01:07:02.142 Exactly. Well, yeah, Open edX, that's traditionally one of its weaknesses with the forms because they have tried to redevelop their own forum, and that is doomed to failure, of course, because that's a whole major project on its own. But one of the changes we're currently working on for edX is to allow to peak other forums and integrate that in Open edX, and this course is one of them. So we could always be one of the pilot courses for that or something.

S1: 01:07:38.404 True. Okay, so we are only three. Is it reasonable to fill the person in charge [inaudible]?

S2: 01:07:47.734 Oh, that's perfect. We can just give that to all the people who did not come today [inaudible] [crosstalk].

S1: 01:07:53.902 [crosstalk]--

S2: 01:07:56.360 Yeah, [inaudible], if you want to take anything, it's up for grabs. Well, we can maybe already assign to ourselves. I think in my document, I pointed to the two parts that I was interested in. I think one was the social nature, but I guess it's an intro, so probably, that's something we could be several on. We'll probably have all stuff to say. But I think the main thing for on my side is mostly just the project tips from the end, basically, what I take from every week. Plus I contributed the project structure, but, yeah, for that week, I think that's the main thing I want to take.

S1: 01:08:41.504 Yeah, I can probably talk about project connectors and licenses and economic models.

S2: 01:08:48.731 Okay, cool.

S1: 01:08:50.664 Which is a lot--

S2: 01:08:52.722 Yeah, it's the bulk of it that week, including the activities, the case studies and stuff.

S1: 01:09:04.512 I will need help for the case studies. I can do the contents passing things, but the activities will be more work probably.

S2: 01:09:18.434 I remember from this transcript that-- and I was really interested in the case studies. So I don't know how deep she knows the actual license and economic models, but at least in terms of-- she had in mind some of the the way they did exercise for business cases. So she might be at least a good contributor on those ones.

S1: 01:09:44.111 Okay. I'll put her with interrogation mark. And trolls and disputes, I will put [inaudible].

S2: 01:10:00.634 Yeah, he was the one bringing that all the time, so.

S1: 01:10:08.944 I will also put a question mark, because he's not here.

S2: 01:10:13.851 Yes, true. Did you manage to talk to him?

S1: 01:10:17.303 Yeah, I talked to him. And apparently that time from 7:00 PM is not so best time for him with his family. And also he wanted an email to remind him because he did not follow any metrics. And he told me it's hard to know when the discussions happened and where, but.

S2: 01:10:49.351 Yes, that is true that-- I mean, it's true that right now people who don't come regularly in meetings are quickly lost, I think, because-- I mean, there are the transcripts, but it's a huge amount of-- it's a wall of text. So I think it's a bit difficult, especially when you start to be two, three meetings behind. Actually, that's a remark I wanted to [inaudible] before today that we are good in the synchronous part because we move forward things a lot, I think, on that front, but not so much on the asynchronous. There are lots of tickets that are kind of stale with some discussions where we haven't really taken a decision.

S2: 01:11:36.426 So I think it might be worth trying to - I don't know - either dedicate some time to that or find a way to organize our time on this to make sure that we close stuff also and we move discussion forward on the asynchronous parts. And I think that would probably make it a lot easier for people who don't come to all the meetings to actually contribute and follow on some of the stuff. I mean, they are the merge requests where some people have been able to contribute. I saw some people from OpenCraft, and I think, Kendall and Indico commented there. But if we could do a bit of that also, on the tickets, I think that might be useful. I don't know exactly how to encourage everyone to do this. But yeah.

S1: 01:12:33.953 Yeah. I also don't know how to end the discussion that's open.

S2: 01:12:42.442 Well, I think in a bunch of cases, because you have a BDF role, once everyone has explained their opinion, if there is not a clear-- I mean, when there is a clear consensus, I think it might help to restate it like, okay, so we seem to all agree on that. And when there is not, to just pick one of the options just to make sure we move forward because otherwise we are just going to be blocked. Think it's the tough job of the BDF also. Good luck with that.

S1: 01:13:13.927 Yeah. Yeah. I chose that for myself, so. [laughter]

S2: 01:13:29.499 All right. Yeah. Are we good [inaudible]? What are we missing? So you took the [inaudible] [Hector's?]. Troll and dispute, that might be [inaudible]. I took project tips. So it's missing the quiz time and I guess the overall coordination of that week, right.

S1: 01:13:54.640 I put myself in the overall coordination because I think I can-- yeah, I'm interested--

S2: 01:14:03.931 You take [crosstalk]--

S1: 01:14:04.960 --in week three and four, mostly, I think for myself. What was week four?

S2: 01:14:12.020 Week four is contribution. That's when I would also be interested in coordinating. But I already have one. So we can always discuss that next time.

S1: 01:14:23.086 I will take the week three because I have most of the content, transmitted content in it. And I will need to coordinate with people on week one, which is you, about interviews, what can we materialize?

S2: 01:14:44.579 Yep. I think for interviews, the ideal would be, all of the sections of the course which have potentially questions for the interviews could simply add a section in that part of the course saying, "Okay. Questions to ask." And then at the end, when we start preparing more seriously the interviews, we would just go through the whole course, pick all of those questions, and make a questionnaire. And then try maybe to assign those questions depending on the person we have in front. So this way that gives kind of a cue where you can put your questions for the interviews without necessarily have to coordinate too much. And at the end, you will be able to review the questions, I think something like that.

S1: 01:15:30.051 Yeah.

S2: 01:15:35.424 The quiz at the end, are you taking that? Or actually what do we have are graded? That might depend on the project tips a bit at some point. But at least a quiz might be good in any case.

S1: 01:15:49.874 Yeah. I think we can mostly grades understanding of project enactors and licenses and economic models which is the most transmittable thing that we can check on.

S2: 01:16:04.823 Sure. I mean, in any case, that will be a good thing to check anyway, so.

S1: 01:16:13.934 [inaudible], what do you think about the contents of this module?

S3: 01:16:19.533 No, I think it's quite clear. And I like the activities, especially the third part. The--

S1: 01:16:30.545 The third part? The case study?

S3: 01:16:33.332 Yeah. I think it may be interesting. I would be interested in doing this as a learner.

S2: 01:16:46.390 Okay. Cool. And are you interested in contributing to any of those things?

S3: 01:16:52.110 I thought of contributing to some section, but I don't feel confident or having the content to explains some things. The easiest for me would have been types of actors, I think, we saw in the second section.

S2: 01:17:18.182 I mean, that depends on you, Mark. But you could always collaborate on that one, and this way, [inaudible], if you don't have everything, you don't have to write everything. But I'm guessing, Mark, it would probably help you. I mean, I get--

S1: 01:17:31.829 Yeah. Definitely. You should start with a first draft of what you think would be interesting and how to present things, and you could start with it and iterate with me after that.

S3: 01:17:44.634 Okay. So I will assign myself to just this one. This way I will gain confidence, and next time, I could do a little more.

S1: 01:17:57.504 Cool.

S2: 01:18:00.355 Yeah, the hardest is to get started usually on those things, so.

S1: 01:18:05.827 Once you start writing, then you realize, "Oh, I'm good at that, and I can continue writing." [laughter] Or you get blocked on a blank page and you're like, "No, never again." [laughter]

S2: 01:18:19.746 But actually, that's where the brainstorms are really useful because there is never actually a blank page. There is lots of discussions and points from others. So I found that really helpful.

S1: 01:18:30.435 Yeah. You start with a brainstorm, and you say, "How am I going to organize all this information? There is too much to say on the subject. Let's make a syllabus." And then you start with a syllabus, and then you fill the blanks between titles with a sentence and then, "Okay. That's it. I wrote it." [laughter]

S2: 01:18:51.776 Pretty much. There has been a lot of information, actually, in those brainstorms. That's really going to help with the quality and the depth of the course at the end.

S1: 01:19:08.537 Yeah. That was helpful. I think that was a good idea. Okay. Do we have anything to add? Next week, we do not have the people from edX, but we--

S2: 01:19:31.265 From OpenStack you mean?

S1: 01:19:33.194 From OpenStack. Sorry. And we will not have the week four next week. We will have a progress report week.

S2: 01:19:44.034 Yep. Completely. Before we leave, actually there is one point I wanted to bring your attention on. I'm not sure we'll be able to stop the work this week, but that's one of the issues. Let me see.

S1: 01:20:02.396 The one about the format of the GitLab contents?

S2: 01:20:05.925 Yes, exactly, because, again, I'm not sure yet - the sprint hasn't started - if we'll be able to fit in this sprint. So that might be pushed back a couple of weeks, if not. But if it fits this week, then it would be important to know that to not end up doing work that has to be redone. So where is that one again? Oh, no, I'm--

S1: 01:20:38.548 It's number nine now. Number nine now.

S2: 01:20:41.079 Yeah. There we go. I think there's--

S1: 01:20:44.285 [inaudible] that earlier.

S2: 01:20:46.795 Oh, you did?

S1: 01:20:47.869 Yeah. Basically, my main point is, we want something simple and that works to start with. I prefer Markdown to HTML because it's more easily editable. And what worried most in the merge request showing how it can be done was the UATs, which were not really friendly. And so I think we can start with something that works and then slowly change things into what can be more friendly. So I'm not opposed to anything that was proposed. I think the plan from Jill can be completely worked on.

S2: 01:21:42.042 Okay. Perfect then. So if that--

S1: 01:21:45.972 What do you think?

S2: 01:21:46.889 Yeah, I mean, on my side, I'm completely fine with any of those options. Anyway, I think keeping it simple is probably one of the main things. Then in terms of opinion, the more we are actually able to use the capabilities of the platform, the better, but I don't have any blogs on that. So Markdowns make sense to me, and there are different ways we can go at that. If what Jill proposed is good for you, then that's good for me because on the technical side, I'll probably completely delegate that to Jill anyway. That's what I tend to do in OpenCraft. I let the technical side being handled by other people, and so on my side, it's all good.

S1: 01:22:40.141 Yeah. And I suppose that the MCQs can also be done in Markdown, so.

S2: 01:22:47.072 On Open edX you mean?

S1: 01:22:51.493 Yeah.

S2: 01:22:52.720 Oh, I've not seen that, but perfect then. That's great. Let me see my list. I think I had a couple of things. Yeah. The merging of the pull requests. So if we can move forward on that, but I think you already did some commands, so that's great. And, yeah. Now, that's it for this week.

S1: 01:23:15.572 Where is the most in this, in the structure of Markdown or Excel is the DI list for our file names, which I think don't really make sense, but what--

S2: 01:23:29.538 Well, the thing is that it doesn't have to be like that because we can also make it whatever names we want. The only thing is that it's Studio, which tends to generate them this way so that's why it tends to-- when you just [inaudible] and export it, that's what you get. But I think you can rework it and have something a bit more meaningful, so maybe that's what we can iterate over the course afterwards is to rename them to make them meaningful or something.

S1: 01:24:02.692 Yeah, but if we rename them and then re-upload the course in bulk, does it have an impact?

S2: 01:24:11.274 I think Studio will keep the DI list that we'll have set. That said, I haven't tried that in some time, so I wouldn't bet my hands to cut on this. I don't even know the expression. But, yeah. I think it's fine, but we'd probably have to test. The worst case that would mean that we would have to choose between having nice UIDs and being able to edit in Studio. But in any case, we can still keep the principle of having that structure there, but we might end up having to take a decision between the two, but I think, actually, we might not. Studio might be able to just keep that.

S1: 01:24:58.927 Well, we can have a CI job that just renames all files according to some rules, but I don't know how Studio generates the UIDs and if it expects that some files has some UIDs, and otherwise, it will generate another, and in this case we cannot really--

S2: 01:25:19.146 I think this part should be fine. Don't quote me on this, but we can test that. Actually, if you have some concern on that, don't hesitate to ask the question in that ticket to Jill, and whoever works on that can have a look at the same time and answer on that.

S1: 01:25:35.834 Okay. All good. I think the sort of priority in that technical aspect is to get the first things that work, and that can be imported in bulk to Open edX. And if we can work on it after that and make it more friendly or more easy to edit, than we can do it after that as long as we edit CI to just generate the same thing at the end. That's my understanding of what should be the process.

S2: 01:26:13.337 Yes. And I think, in any case, Studio should be able to take the remained stuff. What I don't know is when you re-export it if it would keep that. So the main thing is how closely are you able to match what you potentially re-export from Studio to what is in Git, but in any case, I think the direction from Git to Studio should be fine.

S1: 01:26:41.596 Yeah. But why would we export from Studio?

S2: 01:26:47.193 It's just that sometimes, you have some stuff that you might want to [inaudible] In Studio and just take the generated [inaudible] from that. So you might not re-import the whole course back into Git, but that's a workflow that we have, for example, on some of our calls to do some of the edits in Studio and then use that course export and copy those files to Git, and then you see the diff, and then you might have some changes to make to edit it to make it fine, but at least you don't have to write the [inaudible] from scratch. But that's a detail. We don't necessarily need to have that, yeah.

S1: 01:27:35.280 I think I understand why. Yeah. And you can import in Studio a whole course, and people who are in the middle of the course won't get lost if it finds the same course at the same place, right?

S2: 01:27:54.500 Yes. I mean, you have to be careful about what you change in a course that's already running. Generally it's good practice to stop making changes to the course once it's running, at least to the sections that have been published already. Generally, it works, but if you - I don't know - replace unit by another, and people were still on that unit, or if there are some exercise that they have already completed or have half done, and then you change the things, that that might create some issues, but generally it's done often enough that most of the cases are fine. You just have to be mindful of the impact on existing users.

S1: 01:28:39.169 Okay. Good. And if it mostly works, then I'm fine.

S2: 01:28:45.258 Yes, mostly works is exactly it.

S1: 01:28:49.830 Okay. So thank you both for attending today. I think if we don't have any other topics linked to the week, then--

S2: 01:29:01.098 Yep. I think that's good, and we'll have the whole session next week to go into details about other parts. All right.

S1: 01:29:10.907 Right. Thank you.

S2: 01:29:11.421 Thank you. Have a good evening.

S1: 01:29:13.195 See you next week.

S2: 01:29:13.997 See you.
